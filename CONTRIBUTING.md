CONTRIBUTING
============

Coding style
------------

We use LLVM style, with a few exceptions. See the [clang-format configuration](./.clang-format) for more about this.

It is possible to let git ensure that you are conforming to the standards by using pre-commit hooks and clang-format:

```
sudo apt-get install clang-format 

# Then in vaRays's home folder:
rm -rf hooks && ln -s $(pwd)/.hooks $(pwd)/.git/hooks
```

Contributing
------------

Please send your merge requests at the [SAT-Metalab's Gitlab account](https://gitlab.com/sat-metalab/vaRays). If you do not know how to make a merge request, gitlab provides some [help about collaborating on projects using issues and pull requests](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).

Branching strategy with git
---------------------------

The [master](https://gitlab.com/sat-metalab/vaRays/tree/master) branch contains vaRays releases. Validated new developments are into the [develop](https://gitlab.com/sat-metalab/vaRays/tree/develop) branch.

Modifications are made into a dedicated branch that need to be merged into the develop branch through a gitlab merge request. When you modification is ready, you need to prepare your merge request as follow:

* Update your develop branch. 
```
git fetch
git checkout develop
git pull origin develop
```
* Go back to your branch and rebase onto develop. Note that it would be appreciated that you only merge request from a single commit (i.e interactive rebase with squash and/or fixup before pushing).
```
git checkout NAME_OF_YOUR_BRANCH
git rebase -i develop
```

How to make new releases
------------------------
- The python [release script](./scripts/Release/release_version.py) takes you through the steps for release. 
- Before running the script, your repo must have a default editor. Run the following command to select a core editor for the git repo:
    ```sh
    git config core.editor "<your_favorite_texteditor(vim/emacs/nano...)>"
    # use the '--global' flag to set the editor as default for all repos
    ```
- Run the release script as follows and follow the prompt:
    ```sh
    ./scripts/Release/release_version.py
    ```
    The script will create a new directory, clone the repo, build and run tests locally before actually making the release. This may take a long time.
- Before your new release is ready to be pushed upstream, the script will open the [NEWS.md](./NEWS.md) on the editor you previously set, with all the merge commits since the last tag. Edit as required, save and close the text editor.
