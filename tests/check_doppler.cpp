//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#undef NDEBUG // get assert in release mode

#include <cassert>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <numeric>

#include "../varays/Auralizer/DopplerEngine/DelayLine.hpp"
#include "../varays/Auralizer/DopplerEngine/DopplerEngine.hpp"

#define myrand rand() / static_cast<float>(RAND_MAX)

using namespace varays;

int main()
{
    srand(static_cast<unsigned>(time(0)));

    // Test delay lines at different lagrange interpolation orders
    int delayLineSize = 2048;
    int frameSize = 256;
    int minNumFrames = 5;
    int maxNumFrames = 10;
    float maxDelayGrowthRate = myrand;
    float maxDelay = static_cast<float>(frameSize - 1);
    // Note: The max delay should ideally be (delayLineSize - frameSize),
    // but in this test we are only concerned with proper wrapping around
    // of the delay write and read pointers.

    int numInputs = 5;
    float maxVelocity = 50.0f;
    float speedOfSound = 343.0f;
    unsigned int sampleRate = 48000;

    std::vector<int> interpOrders{0, 1, 2, 3, 4, 5};
    for (const auto& order : interpOrders)
    {
        DelayLine dLine(maxDelayGrowthRate, order, 0.0f, delayLineSize);
        std::vector<float> delayIn(frameSize, 0.0f);
        std::vector<float> delayOut(frameSize, 0.0f);

        // Set delay with instantaneous effect and check if it is properly set
        float d1 = myrand * (maxDelay - static_cast<float>(order / 2));
        dLine.setDelay(d1);
        assert(std::abs(dLine.getCurrentDelay() - d1) < __FLT_EPSILON__);

        // Update delay over a random number of samples and check if it is updated as expected
        float d2 = myrand * (maxDelay - static_cast<float>(order / 2));
        int updDur = 0;
        while (updDur < 10) // update over a random number of steps greater than 10 samples
            updDur = std::abs((d1 - d2) / ((myrand * 2.0f - 1.0f) * maxDelayGrowthRate));
        dLine.setDelay(d2, updDur);

        // Tick in a few samples and test again
        for (int i = 0; i < updDur; i += frameSize)
        {
            assert(std::abs(dLine.getCurrentDelay() - d2) > __FLT_EPSILON__);
            dLine.writeSamples(delayIn.data(), std::min(updDur - i, frameSize));
            dLine.readSamples(delayOut.data(), std::min(updDur - i, frameSize));
        }
        assert(std::abs(dLine.getCurrentDelay() - d2) < __FLT_EPSILON__);

        // Explicit test
        // Test if the read pointer correctly wraps around the delay line. This is prone to failure.
        // Conditions: Delay between (order/2) and (order) number of samples and read pointer in the beginning of the delay line.
        dLine.setDelay((myrand + 1.0f) * static_cast<float>(order) / 2.0f);
        delayIn[0] = 1.0f;
        dLine.writeSamples(delayIn);
        dLine.readSamples(delayOut);
        float sum = 0.0f;
        for (const auto& samp : delayOut)
            sum += samp;
        assert(std::pow(sum - 1.0f, 2.0f) < __FLT_EPSILON__); // Compare the square of the error to avoid floating-point errors

        // Test DopplerEngine class
        DopplerEngine dopplerShifter(1, order, maxVelocity, speedOfSound);
        dopplerShifter.setAudioDeviceProperties(sampleRate, frameSize);

        // Create inputs and set delays
        for (int inp = 0; inp < numInputs; ++inp)
        {
            dopplerShifter.inputCreate("TestInput" + std::to_string(inp));
            // The resulting delay must be between order/2 and maxDelay - order/2
            // to avoid losing interpolated samples at the ends of the buffer for comparison.
            float length = (myrand * (maxDelay - order) + static_cast<float>(order) / 2) * speedOfSound / sampleRate;
            dopplerShifter.updateSource("TestInput" + std::to_string(inp), {SoundEvent::SoundEventConfig(inp, length, {}, {1}, {1, 0, 0}, {})}, true, true);
        }

        // The interpolated delay line implementation is lossy due to its low pass behaviour,
        // i.e., the output signal energy is less than or equal to the input signal energy.
        // Hence, we cannot compare energies of the input and output signals to validate the delay line.
        // We may however compare the sum of the sample values of the input and output signals since
        // the Lagrange interpolation filters sum to 1.
        for (auto& samp : delayIn)
            samp = myrand * 2.0f - 1.0f; // Signal value between -1 and +1
        float inpSum = std::accumulate(delayIn.begin(), delayIn.end(), 0.0f);

        int numFrames = rand() % (maxNumFrames - minNumFrames) + minNumFrames;
        float dOutSum = 0.0f;
        for (int i = 0; i <= numFrames; ++i)
        {
            if (i == numFrames)
                std::fill(delayIn.begin(), delayIn.end(), 0.0f);
            for (int inp = 0; inp < numInputs; ++inp)
                dopplerShifter.loadInputBlock("TestInput" + std::to_string(inp), delayIn);
            dopplerShifter.process();
            delayOut = dopplerShifter.getOutputBlock(0);
            assert(delayOut.size() == static_cast<size_t>(frameSize));
            dOutSum = std::accumulate(delayOut.begin(), delayOut.end(), dOutSum);
        }
        auto avgSumPerFramePerInput = dOutSum / (numFrames * numInputs);
        assert(std::pow(avgSumPerFramePerInput - inpSum, 2.0f) < __FLT_EPSILON__);
    }
    return 0;
}