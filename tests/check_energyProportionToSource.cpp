#undef NDEBUG // get assert in release mode

#include <Eigen/Dense>

#include "../varays/Context/RayTracer/utils.hpp"
#include "./testUtils.hpp"

using namespace varays;

int main()
{
    for (int i = 0; i < 1000; ++i)
    {
        Eigen::Vector3f normal = testUtils::randVec3();
        Eigen::Vector3f dir = testUtils::randVec3();
        if (dir.dot(normal) >= 0)
            dir = Eigen::Vector3f(-dir[0], -dir[1], -dir[2]);

        Eigen::Vector3f sourcePos = testUtils::randVec3();
        Eigen::Vector3f originPos = testUtils::randVec3();
        Eigen::Vector3f toSrcDir = sourcePos - originPos;

        if (toSrcDir.dot(normal) < 0)
        {
            Eigen::Vector3f tmp = sourcePos;
            sourcePos = originPos;
            originPos = tmp;
        }

        // create the TreeNode
        utils::SpecAndDiff energyProportions = utils::energyProportionToSource(testUtils::unitRandFloat() * 5.f, originPos, sourcePos, dir, normal);
        assert(energyProportions.specular >= 0.f && energyProportions.specular <= 1.f);
        assert(energyProportions.diffuse >= 0.f && energyProportions.diffuse <= 1.f);
    }
    return 0;
}