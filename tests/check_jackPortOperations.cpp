//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#undef NDEBUG // get assert in release mode

#include <algorithm>
#include <cassert>
#include <cmath>
#include <stdlib.h>
#include <string>
#include <time.h>

#include <jack/jack.h>

#include "../varays/Auralizer/VaraysAuralizer.hpp"

int main()
{
    srand(time(nullptr));
    int maxInps = std::max(rand() % 10, 2);
    int maxOuts = std::max(rand() % 10, 2);

    // check if convolver is created properly
    VaraysAuralizer::VaraysAuralizerParams convParams;
    convParams.numInputs_ = maxInps;
    convParams.numOutputs_ = maxOuts;
    VaraysAuralizer convolver(convParams);
    assert(convolver.isAllocated()); // test if constructor successfully executed
    std::string jackClientName = convolver.getJackClientName();

    // check creation of outputs
    jack_client_t* jclient = convolver.getJackClientPtr();
    const char** jackOutputPorts = jack_get_ports(jclient, jackClientName.c_str(), "", JackPortIsOutput);
    int numOutputs = 0;
    while (jackOutputPorts[numOutputs] != nullptr)
        numOutputs++;
    jack_free(jackOutputPorts);
    assert(numOutputs == maxOuts); // check if all the outputs are created successfully

    // check creation of inputs
    std::vector<std::string> inputPorts;
    for (int i = 0; i < maxInps; ++i)
    {
        inputPorts.push_back(std::string("dummyInput" + std::to_string(i)));
        convolver.newSource(inputPorts[i]);
    }
    const char** jackInputPorts = jack_get_ports(jclient, jackClientName.c_str(), "", JackPortIsInput);
    int numInputs = 0;
    while (jackInputPorts[numInputs] != nullptr)
    {
        std::string inPort(jackInputPorts[numInputs]);
        assert(jackClientName + ":" + inputPorts[numInputs] == inPort);
        numInputs++;
    }
    jack_free(jackInputPorts);
    assert(numInputs == maxInps);

    // check deletion of inputs
    int numDels = std::max(rand() % maxInps, 1);
    std::vector<std::string> deletedInputs;
    for (int i = 0; i < numDels;)
    {
        int inpToDelete = rand() % maxInps;
        auto it = std::find(deletedInputs.begin(), deletedInputs.end(), jackClientName + ":" + inputPorts[inpToDelete]);
        if (it == deletedInputs.end())
        {
            convolver.deleteSource(inputPorts[inpToDelete]);
            deletedInputs.push_back(jackClientName + ":" + inputPorts[inpToDelete]);
            ++i;
        }
    }
    jackInputPorts = jack_get_ports(jclient, jackClientName.c_str(), "", JackPortIsInput);
    numInputs = 0;
    while (jackInputPorts[numInputs] != nullptr)
    {
        std::string inPort(jackInputPorts[numInputs]);
        auto it = std::find(deletedInputs.begin(), deletedInputs.end(), inPort);
        assert(it == deletedInputs.end());
        numInputs++;
    }
    jack_free(jackInputPorts);
    assert(numInputs == maxInps - numDels);

    return 0;
}
