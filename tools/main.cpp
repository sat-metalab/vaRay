// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Std includes
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <memory>
#include <random>
#include <signal.h>
#include <string>
#include <time.h>
#include <unistd.h>

// Lib includes
#include <Eigen/Dense>
#include <embree3/rtcore.h>
#include <lo/lo.h>
#include <sndfile.h>

// Varays
#include "varays/Auralizer/VaraysAuralizer.hpp"
#include "varays/Context/RayTracer/utils.hpp"
#include "varays/Context/VaraysContext.hpp"
#include "varays/Encoder/VaraysEncoder.hpp"
#include "varays/Utils/JsonSave.hpp"
#include "varays/Utils/periodic-task.hpp"

using namespace varays;

Logger logger_("vaRaysApplication");
std::unique_ptr<VaraysContext> context_;
std::unique_ptr<VaraysEncoder> encoder_;
std::unique_ptr<VaraysAuralizer> convolver_;

namespace varaysForOSC
{

// dealing with received OSC:
std::map<std::string, Eigen::Vector3f> sources_to_add;
std::vector<std::string> sources_to_remove;
bool remove_all_sources = false;
std::mutex sources_mtx;

Eigen::Vector3f listener_pos;
Eigen::Vector3f listener_pos_cache;

// OSC to SATIE:
lo_address satie_osc;

struct Params : public JsonSerializable
{
    bool enableAuralizer_{false};
    std::string jackServerName_{"default"};

    bool enableDopplerShift_{true};
    unsigned int delayLineInterpolationOrder_{2};
    float maxRelativeVelocity_{50.0f};

    int ambiOrder_{0};
    int maxAudioSrc_{5};
    int sampleRate_{48000}; // Sampling frequency for the generated IR
    double temperature_{20.0}; // celsius
    float humidity_{50.0}; // %
    float pressure_{101.325}; // kPa

    bool enableRayCache_{false};
    bool enableDiffraction_{false};
    int initRays_{500}; // Amount of "original" rays shot from the user location
    int numReflRays_{3}; // Amount of reflected rays for every reflection on a surface
    int maxReflOrder_{8}; // Maximum amount of reflections
    int maxDiffuseOrder_{3}; // After this amount of reflections, all reflections will only be specular
    float minEnergyThreshold_{0.005}; // Threshold of energy (fraction of its initial ray) a ray must have in order to be thrown
    int delay_{100}; // Minimum time (ms) taken before generating a new IR (it can take longer if the calculations take too long)

    std::string oscInPort_{"9000"};
    std::string oscOutPort_{"7770"};

    std::string irPath_{""};
    std::string rayObjPath_{""};
    std::string objFile_{""}; // Path of the obj file used as a scene

    Params() {}
    Params(const std::string configPath) { readConfigFile(configPath); }

    std::string type_string() const // for identifying class name in json file
    {
        return "Params";
    }

    void serialize(Json::Value& root) const {}

    void deserialize(const Json::Value& root)
    {
        enableAuralizer_ = root.get("EnableAuralizer", false).asBool();
        jackServerName_ = root.get("JackServerName", "default").asString();

        enableDopplerShift_ = root.get("EnableDopplerShift", true).asBool();
        delayLineInterpolationOrder_ = root.get("DelayLineInterpolationOrder", 2).asUInt();
        maxRelativeVelocity_ = root.get("MaxRelativeVelocity", 50.0f).asFloat();

        ambiOrder_ = root.get("AmbisonicOrder", 0).asInt();
        maxAudioSrc_ = root.get("MaxAudioSources", 5).asInt();
        sampleRate_ = root.get("SampleRate", 48000).asInt();

        temperature_ = root.get("Temperature", 20.0).asDouble();
        humidity_ = root.get("Humidity", 50.0).asFloat();
        pressure_ = root.get("Pressure", 101.325).asFloat();

        enableRayCache_ = root.get("EnableRayCache", false).asBool();
        enableDiffraction_ = root.get("EnableDiffraction", false).asBool();
        initRays_ = root.get("InitRays", 500).asInt();
        numReflRays_ = root.get("NumReflectedRays", 3).asInt();
        maxReflOrder_ = root.get("MaxReflectionOrder", 8).asInt();
        maxDiffuseOrder_ = root.get("MaxDiffuseOrder", 3).asInt();
        minEnergyThreshold_ = root.get("MinEnergyThreshold", 0.005).asFloat();
        delay_ = root.get("RTPeriod", 100).asInt();

        oscInPort_ = root.get("OSCInPort", "9000").asString();
        oscOutPort_ = root.get("OSCOutPort", "7770").asString();

        irPath_ = root.get("IRPath", "").asString();
        rayObjPath_ = root.get("RayObjPath", "").asString();
        objFile_ = root.get("ObjFile", "").asString();
    }

    void readConfigFile(const std::string& configPath)
    {
        std::ifstream ifile;
        ifile.open(configPath);
        if (ifile.is_open())
        {
            std::string config_fromJsonFile((std::istreambuf_iterator<char>(ifile)), (std::istreambuf_iterator<char>()));
            if (!jsonDeserialize<Params>(this, config_fromJsonFile))
                logger_.log(Logger::ERROR, "Unable to parse configuration file. Using defaults.");
        }
        else
        {
            logger_.log(Logger::ERROR, "Configuration file not found. Using defaults.");
        }
        ifile.close();
    }
};

void error(int num, const char* msg, const char* path)
{
    logger_.log(Logger::CRITICAL, "Unable to create port for OSC messages.");
}

int stopLoop(const char* path, const char* types, lo_arg** argv, int argc, void* data, void* user_data)
{
    *static_cast<bool*>(user_data) = false;
    return 1;
}

int rdObj(const char* path, const char* types, lo_arg** argv, int argc, void* data, void* user_data)
{
    context_->readObjFile(std::string(&(argv[0]->s)), false);
    return 1;
}

void addCube(bool diffraction = true, const Eigen::Vector3f& center = {.0f, .0f, .0f}, float length = 1.0f)
{
    auto vertices = std::vector<Eigen::Vector3f>();

    for (int i = 0; i < 8; ++i)
    {
        vertices.push_back(center + length / 2 * Eigen::Vector3f(pow(-1.f, i / 4), pow(-1.f, i / 2), pow(-1.f, i)));
    }

    std::vector<int> indices{0, 1, 2, 1, 3, 2, 4, 6, 5, 5, 6, 7, 0, 4, 1, 1, 4, 5, 2, 3, 6, 3, 7, 6, 0, 2, 4, 2, 6, 4, 1, 5, 3, 3, 5, 7};

    context_->addGeom(vertices, indices, diffraction);
}

void computeIR(Params* prmtr)
{
    { // add/remove sources
        std::lock_guard<std::mutex> lock(sources_mtx);
        for (const auto& it : sources_to_add)
        {
            context_->addSource(it.second, it.first);
            if (prmtr->enableAuralizer_)
            {
                convolver_->newSource(it.first);
                if (prmtr->enableDopplerShift_)
                {
                    int sourceID = context_->getSourceIDFromName(it.first);
                    auto src = context_->getSource(sourceID);
                    auto soundEvent = context_->computeDirectSoundEvent(sourceID);
                    convolver_->updateDelayLines(src.getName(), soundEvent);
                }
            }
        }
        sources_to_add.clear();
        if (remove_all_sources)
        {
            for (auto& source : context_->getSources())
                sources_to_remove.emplace_back(source.getName());
            remove_all_sources = false;
        }
        for (const auto& it : sources_to_remove)
        {
            context_->removeSource(context_->getSourceIDFromName(it));
            if (prmtr->enableAuralizer_)
                convolver_->deleteSource(it);
        }
        sources_to_remove.clear();
    } // end of context release lock

    if (context_->getSources().empty())
        return;

    // Launch rays
    std::vector<SoundEvent> soundEvents = context_->executeRayManagers(prmtr->initRays_, prmtr->maxDiffuseOrder_, prmtr->numReflRays_);

    if (soundEvents.empty())
        return;

    // [Source][Frequency][frame]
    auto IRForSources = encoder_->writeIR(prmtr->ambiOrder_, soundEvents, !prmtr->enableDopplerShift_);

    // Update IR for all sources
    if (prmtr->enableAuralizer_)
    {
        auto srcs = context_->getSources();
        for (auto& ir : IRForSources)
        {
            auto srcIt = std::find_if(srcs.begin(), srcs.end(), [&](auto& src) -> bool { return src.getId() == ir.first; });
            if (srcIt != srcs.end())
            {
                std::vector<float> newIR(ir.second.begin(), ir.second.end());
                convolver_->updateIR(srcIt->getName(), std::pow(prmtr->ambiOrder_ + 1, 2), newIR);
            }
        }
    }

    // Save IR in files
    if (!prmtr->irPath_.empty())
    {
        auto srcs = context_->getSources();
        for (auto& ir : IRForSources)
        {
            auto srcIt = std::find_if(srcs.begin(), srcs.end(), [&](auto& src) -> bool { return src.getId() == ir.first; });
            if (srcIt != srcs.end())
            {
                auto path = prmtr->irPath_ + std::string(srcIt->getName());
                encoder_->exportIR(ir.second, path, pow(prmtr->ambiOrder_ + 1, 2), prmtr->sampleRate_);
                if (lo_send(satie_osc, "/ir_file", "s", path.c_str()) == -1)
                    lo_address_errstr(satie_osc);
            }
        }
    }

    // Export rays to .obj
    if (!prmtr->rayObjPath_.empty())
        context_->writeRaysToObj(prmtr->rayObjPath_);
}

int mvLis(const char* path, const char* types, lo_arg** argv, int argc, void* data, void* user_data)
{
    listener_pos = Eigen::Vector3f(argv[0]->f, argv[1]->f, argv[2]->f);
    context_->moveListener(listener_pos);
    if (!convolver_)
        return 1;
    for (auto& src : context_->getSources())
    {
        if (convolver_->dopplerEnabled())
        {
            auto soundEvent = context_->computeDirectSoundEvent(src.getId());
            convolver_->updateDelayLines(src.getName(), soundEvent);
        }
    }
    return 1;
}

int addSource(const char* path, const char* types, lo_arg** argv, int argc, void* data, void* user_data)
{
    std::lock_guard<std::mutex> lock(sources_mtx);
    sources_to_add[std::string(&(argv[0]->s))] = Eigen::Vector3f(argv[1]->f, argv[2]->f, argv[3]->f);
    return 1;
}

int removeSource(const char* path, const char* types, lo_arg** argv, int argc, void* data, void* user_data)
{
    std::lock_guard<std::mutex> lock(sources_mtx);
    sources_to_remove.push_back(std::string(&(argv[0]->s)));
    return 1;
}

int removeAllSources(const char* path, const char* types, lo_arg** argv, int argc, void* data, void* user_data)
{
    remove_all_sources = true;
    return 1;
}

int moveSrc(const char* path, const char* types, lo_arg** argv, int argc, void* data, void* user_data)
{
    auto srcName = std::string(&(argv[0]->s));
    auto srcPos = Eigen::Vector3f(argv[1]->f, argv[2]->f, argv[3]->f);
    auto srcID = context_->getSourceIDFromName(srcName);
    if (!context_->moveSource(srcID, srcPos))
        return 1;
    if (convolver_ && convolver_->dopplerEnabled())
    {
        auto src = context_->getSource(srcID);
        auto soundEvent = context_->computeDirectSoundEvent(srcID);
        convolver_->updateDelayLines(src.getName(), soundEvent);
    }
    return 1;
}

}; // namespace varaysForOSC

lo_server_thread st;

void leave(int sig)
{
    logger_.log(Logger::INFO, "Cleaning liblo threads before quitting...");
    lo_server_thread_free(st);
    exit(sig);
}

int main(int argc, char* argv[])
{
    (void)signal(SIGINT, leave);
    (void)signal(SIGABRT, leave);
    (void)signal(SIGQUIT, leave);
    (void)signal(SIGTERM, leave);

    int opt;
    std::map<char, std::string> inputArgs;
    while ((opt = getopt(argc, argv, "hCj:a:i:r:o:c:")) != -1)
    {
        if (!optarg)
            inputArgs[opt] = "";
        else
            inputArgs[opt] = optarg;
    }

    varaysForOSC::Params prmtr(std::string(DATADIR) + "/Config/vaRaysAppConfig.json");
    if (auto conf = inputArgs.find('c'); conf != inputArgs.end())
    {
        prmtr.readConfigFile(conf->second);
        inputArgs.erase(conf);
    }

    for (const auto& iArg : inputArgs)
    {
        switch (iArg.first)
        {
        case 'h': // help
            std::cout << std::endl;
            std::cout << "\033[1;34m        - vaRays - for OSC - \033[0m\n";
            std::cout << std::endl;
            std::cout << "Options without argument:\n";
            std::cout << "      -h: help\n";
            std::cout << "      -C: Enable varays auralizer (Requires an active Jack server)\n";
            std::cout << std::endl;
            std::cout << "Options with argument:\n";
            std::cout << "      -c: Read from configuration file\n";
            std::cout << "      -j: Name of the jack server (Default = \"default\")\n";
            std::cout << "      -a: Ambisonic order of the output (uses SN3D) (Default = 0)\n";
            std::cout << "      -i: Relative path of the generated IR file (Default in config file)\n";
            std::cout << "      -r: Relative path of the .obj file containing rays visible to the sources (Default in config file)\n";
            std::cout << "      -o: Path of the used obj file used as a scene (Default = "
                         ", will use a 2x2x2 cube centered in (0,0,0))\n";
            std::cout << std::endl;
            exit(0);
        case 'j':
            prmtr.jackServerName_ = iArg.second;
            break;
        case 'a': // ambiOrder
            prmtr.ambiOrder_ = atoi(iArg.second.c_str());
            break;
        case 'i': // ir output path
            prmtr.irPath_ = iArg.second;
            break;
        case 'r': // ray obj output path
            prmtr.rayObjPath_ = iArg.second;
            break;
        case 'o': // objFile
            prmtr.objFile_ = iArg.second;
            break;
        case 'C': // auralizer
            prmtr.enableAuralizer_ = true;
            break;
        }
    }

    context_ = std::make_unique<VaraysContext>(
        std::string(std::string(DATADIR) + "/Config/materials.json"), Eigen::Vector3f(-.5f, 1.0f, -1.5f), prmtr.enableRayCache_);
    encoder_ = std::make_unique<VaraysEncoder>(context_->getFrequencies(), prmtr.sampleRate_);
    encoder_->setAtmosphericConditions(prmtr.humidity_, prmtr.pressure_, prmtr.temperature_);

    if (prmtr.enableAuralizer_)
    {
        VaraysAuralizer::VaraysAuralizerParams convParams;
        convParams.jackServerName_ = prmtr.jackServerName_;
        convParams.numInputs_ = prmtr.maxAudioSrc_;
        convParams.numOutputs_ = std::pow(prmtr.ambiOrder_ + 1, 2);
        convParams.convSize_ = 2 * prmtr.sampleRate_;
        convParams.enableDoppler_ = prmtr.enableDopplerShift_;
        convParams.dopplerInterpOrder_ = prmtr.delayLineInterpolationOrder_;
        convParams.dopplerMaxVelocity_ = prmtr.maxRelativeVelocity_;
        convParams.dopplerSpeedOfSound_ = encoder_->getSpeedOfSound();

        convolver_ = std::make_unique<VaraysAuralizer>(convParams);
        // Check if construction of convolver_ succeeded:
        // First check if unique_ptr is valid (not null).
        // If so, check if VaraysAuralizer is falsy (checks VaraysAuralizer::isReady_ to see if allocation succeeded)
        if ((!convolver_) || (!convolver_->isAllocated())) // construction of convolver_ failed
        {
            logger_.log(Logger::CRITICAL, "Construction of VaraysAuralizer failed.");
            leave(-1);
        }
        convolver_->setVectorizedOperations(true); // Enable SIMD vectorized MAC operations in the convolver
        prmtr.sampleRate_ = static_cast<int>(convolver_->getSampleRate()); // overwritten by jack process
        encoder_->setSampleRate(prmtr.sampleRate_); // Update sample rate of the encoder. Triggers recomputation of filter coefficients.
    }
    else
    {
        prmtr.enableDopplerShift_ = false;
        if (prmtr.irPath_.empty())
            logger_.log(Logger::WARNING, "IRs will not be saved since no path is provided.");
        if (prmtr.rayObjPath_.empty())
            logger_.log(Logger::WARNING, ".obj file containing visible rays will not be saved since no path is provided.");
    }

    bool loop = true;

    if (prmtr.objFile_ == "")
        varaysForOSC::addCube(prmtr.enableDiffraction_, {.0f, .0f, .0f}, 1.0f);
    else
        context_->readObjFile(prmtr.objFile_, prmtr.enableDiffraction_);

    varaysForOSC::satie_osc = lo_address_new(nullptr, prmtr.oscOutPort_.c_str());

    // the following server can be tested using this command (install liblo-tools)
    // $ oscsend localhost 7770 /quit
    st = lo_server_thread_new(prmtr.oscInPort_.c_str(), varaysForOSC::error);
    lo_server_thread_add_method(st, "/quit", "", varaysForOSC::stopLoop, &loop);
    // lo_server_thread_add_method(st, "/context/obj", "s", cppExample::rdObj, &prmtr);
    lo_server_thread_add_method(st, "/editor_position", "fff", varaysForOSC::mvLis, nullptr);
    lo_server_thread_add_method(st, "/new_source", "sfff", varaysForOSC::addSource, nullptr);
    lo_server_thread_add_method(st, "/remove_source", "s", varaysForOSC::removeSource, nullptr);
    lo_server_thread_add_method(st, "/remove_all_sources", "", varaysForOSC::removeAllSources, nullptr);
    lo_server_thread_add_method(st, "/move_source", "sfff", varaysForOSC::moveSrc, nullptr);

    // compute new impulse responses each 100 milliseconds:
    using namespace std::chrono_literals;
    PeriodicTask<> compute_ir([&]() { varaysForOSC::computeIR(&prmtr); }, std::chrono::milliseconds(prmtr.delay_));
    lo_server_thread_start(st);
    // waiting for end of life
    timespec delay;
    delay.tv_sec = 1;
    delay.tv_nsec = 0;
    while (loop)
    {
        nanosleep(&delay, nullptr);
    }
}
