# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import List
import liblo, sys


class OscSender:
    def __init__(self, port: int):
        self._port = port
        self._target = liblo.Address(port)
        self._sources = []

    def send_user_position(self, x: float, y: float, z: float):
        liblo.send(self._target, "/editor_position", x, y, z)
        print("User position changed to", x, y, z, "\n")

    def create_new_source(self, id: str, x: float, y: float, z: float):
        if id not in self._sources:
            liblo.send(self._target, "/new_source", str(id), x, y, z)
            self._sources.append(str(id))
            print("created new source", id, "at", x, y, z, "\n")
        else:
            print("A source named", id, " already exists!\n")

    def remove_source(self, id: str):
        if id in self._sources:
            self._sources.remove(id)
            liblo.send(self._target, "/remove_source", str(id))
            print("Removed source", id, "\n")
        else:
            print("There is no source named", id, "\n")

    def remove_all_sources(self):
        liblo.send(self._target, "/remove_all_sources")
        print("Removed all sources")

    def move_source(self, id: int, x: float, y: float, z: float):
        if id in self._sources:
            # change if vaRays main
            liblo.send(self._target, "/move_source", id, x, y, z)
            print("Moved source", id, "at", x, y, z, "\n")
        else:
            print("There is no source named", id, "\n")

    def quit(self):
        liblo.send(self._target, "/quit")
