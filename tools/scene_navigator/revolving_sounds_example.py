# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# This example python script creates two sources, soundL at (-2.0, 0.5, 0.5) and soundR at (2.0, 0.5, 0.5),
# and moves it in circles around the listener which is at (0.0, 1.0, 1.0) at an angular rate of 0.6 rpm.

import math
from osc_sender import OscSender
import signal
import time

sender = OscSender(9000)
sender.send_user_position(0.0, 1.0, 1.0)
time.sleep(0.15)

sender.create_new_source("soundL", -2.0, 0.5, 2.5)
sender.create_new_source("soundR", 2.0, 0.5, 2.5)

period = 0.1
rotRate = 0.6
angDel = 2 * math.pi * rotRate * period
radius = 2
theta1 = 0
theta2 = math.pi

userInput = input("Press enter for the sources to start revolving around the listener... You can exit the program by pressing Ctrl + c.")
def quit_example(sig, frame):
    sender.remove_source("soundL")
    sender.remove_source("soundR")
    print("Exiting...")
    exit(0)
signal.signal(signal.SIGINT, quit_example)

while 1:
    theta1 += angDel
    theta2 += angDel
    sender.move_source("soundL", radius * float(math.cos(theta1)), radius * float(math.sin(theta1)), 1.0)
    sender.move_source("soundR", radius * float(math.cos(theta2)), radius * float(math.sin(theta2)), 1.0)
    time.sleep(period)
