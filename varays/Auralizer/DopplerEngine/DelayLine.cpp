// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <algorithm>
#include <cmath>

#include "./DelayLine.hpp"

namespace varays
{

DelayLine::DelayLine(float maxDelayGrowthRate, int lagrangeFilterOrder, float dSamp, int len)
    : dLineLength_(len)
    , maxDelayGrowthRate_(maxDelayGrowthRate)
    , lagrangeFilterOrder_(lagrangeFilterOrder)
    , halfLagrangeFilterOrder_(static_cast<float>(lagrangeFilterOrder_) * 0.5f)
{
    dLine_.resize(dLineLength_);
    setDelay(dSamp);

    // Initialize Lagrange filter
    lagrangeFilterDenom_.resize(lagrangeFilterOrder_ + 1, 1.0f);
    lagrangeFilter_.resize(lagrangeFilterOrder_ + 1, 1.0f);
    for (int n = 0; n <= lagrangeFilterOrder_; ++n)
    {
        for (int k = 0; k <= lagrangeFilterOrder_; ++k)
        {
            if (k == n)
                continue;

            lagrangeFilterDenom_[n] /= (n - k);
        }
    }
    updateLagrangeFilter(0.0f);
}

void DelayLine::setDelay(float dSamp, unsigned int updDuration)
{
    if (dSamp < 0.0f || dSamp > dLineLength_)
    {
        logger_.log(Logger::WARNING, "Unable to update delay length. Out of range.");
        return;
    }

    targetDelay_ = dSamp;
    if (updDuration == 0)
    {
        curDelay_ = targetDelay_;
        updateStepsLeft_ = 1; // set this to 1 to trigger recomputation of interpolation filter
    }
    else
    {
        delayIncrement_ = (targetDelay_ - curDelay_) / updDuration;
        updateStepsLeft_ = updDuration;
        if (std::abs(delayIncrement_) > maxDelayGrowthRate_)
        {
            delayIncrement_ = std::min(std::max(delayIncrement_, -maxDelayGrowthRate_), maxDelayGrowthRate_);
            updateStepsLeft_ = static_cast<unsigned int>((targetDelay_ - curDelay_) / delayIncrement_);
        }
    }
}

void DelayLine::writeSamples(const float* inBlock, int blockLen)
{
    for (int i = 0; i < blockLen; ++i)
        tickIn(inBlock[i]);
}

void DelayLine::writeSamples(const std::vector<float>& inBlock)
{
    for (size_t i = 0; i < inBlock.size(); ++i)
        tickIn(inBlock[i]);
}

void DelayLine::readSamples(float* outBlock, int blockLen)
{
    for (int i = 0; i < blockLen; ++i)
        outBlock[i] = tickOut();
}

void DelayLine::readSamples(std::vector<float>& outBlock)
{
    for (size_t i = 0; i < outBlock.size(); ++i)
        outBlock[i] = tickOut();
}

void DelayLine::tickIn(float in)
{
    writePtr_++;
    if (writePtr_ == dLineLength_)
        writePtr_ = 0;

    dLine_[writePtr_] = in;

    numTickedIn_++;
    if (numTickedIn_ >= dLineLength_)
        logger_.log(Logger::WARNING, "Delay buffer overflow!");
}

float DelayLine::tickOut()
{
    if (numTickedIn_ == 0)
    {
        logger_.log(Logger::WARNING, "No new samples to be processed!");
        return 0;
    }

    numTickedIn_--;
    if (updateStepsLeft_ > 0)
    {
        updateStepsLeft_--;
        curDelay_ = targetDelay_ - delayIncrement_ * static_cast<float>(updateStepsLeft_);
        float readPtr = static_cast<float>(writePtr_) - numTickedIn_ - curDelay_;
        updateLagrangeFilter(readPtr - std::floor(readPtr));
        float windowBeg = readPtr - halfLagrangeFilterOrder_;
        if (windowBeg < 0.0f)
            windowBeg += static_cast<float>(dLineLength_);
        interpWindowBeg_ = std::round(windowBeg);
    }
    else
    {
        interpWindowBeg_++;
    }
    if (interpWindowBeg_ >= dLineLength_)
        interpWindowBeg_ -= dLineLength_;

    float sampOut = 0.0f;
    unsigned int sInd = interpWindowBeg_;
    for (const auto& coeff : lagrangeFilter_)
    {
        sampOut += (dLine_[sInd++] * coeff);
        if (sInd == dLineLength_)
            sInd = 0;
    }
    return sampOut;
}

void DelayLine::updateLagrangeFilter(float frac)
{
    lagrangeFilter_ = lagrangeFilterDenom_;

    if (frac < 0.0f || frac >= 1)
        frac = 0.5;

    // centre the interpolating sample around the centre of the filter
    float interpFrac = 0.5f * (lagrangeFilterOrder_ - 1) + frac;

    for (int n = 0; n <= lagrangeFilterOrder_; ++n)
    {
        for (int k = 0; k <= lagrangeFilterOrder_; ++k)
        {
            if (k == n)
                continue;

            lagrangeFilter_[n] *= (interpFrac - k);
        }
    }
}

}; // namespace varays