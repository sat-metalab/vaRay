// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef __DELAYLINE__
#define __DELAYLINE__

#include <vector>

#include "../../Utils/Logger.hpp"

namespace varays
{

class DelayLine
{
  public:
    /*
     * Configure delay line
     *
     * @param   maxDelayGrowthRate  Maximum rate of change of delay. Any changes in delay greater than this rate will be instantaneous.
     * @param   lagrangeFilterOrder Order of Lagrange interpolation filter
     * @param   dSamp               Delay in samples
     * @param   len                 Length of the delay line
     */
    DelayLine(float maxDelayGrowthRate, int lagrangeFilterOrder = 1, float dSamp = 0, int len = 96000);
    /*
     * Update delay.
     *
     * @param dSamp       Delay in samples
     * @param updDuration Number of samples over which the delay is to be updated
     */
    void setDelay(float dSamp, unsigned int updDuration = 0);
    /*
     * Write a block of samples into the delay line.
     *
     * @param   inBlock     Block of input samples
     * @param   blockLen    Number of samples in the block
     */
    void writeSamples(const float* inBlock, int blockLen);
    /*
     * Write a block of samples into the delay line.
     *
     * @param   inBlock     Vector containing block of input samples
     */
    void writeSamples(const std::vector<float>& inBlock);
    /*
     * Read delayed samples from the delay line
     *
     * This function reads samples from the delay line after interpolation.
     *
     * @param   outBlock    Block of delayed samples from the delay line
     * @param   blockLen    Number of samples to read
     */
    void readSamples(float* outBlock, int blockLen);
    /*
     * Read delayed samples from the delay line
     *
     * This function reads samples from the delay line after interpolation.
     *
     * @param   outBlock    Vector containing delayed samples from the delay line
     */
    void readSamples(std::vector<float>& outBlock);

    float getCurrentDelay() { return curDelay_; };

  private:
    std::vector<float> dLine_; /**< Delay buffer*/

    unsigned int dLineLength_{96000}; /**< Length of the delay line in samples*/
    float curDelay_{0}; /**< Current delay in samples*/
    float targetDelay_{0}; /**< Target delay in samples*/
    unsigned int writePtr_{0}; /**< Delay line write pointer*/
    unsigned int interpWindowBeg_{0}; /** Starting sample of the interpolation window*/
    float delayIncrement_{0.0f}; /**< Change in read pointer in samples*/
    float maxDelayGrowthRate_{0.0f}; /**< Maximum rate of delay update*/
    unsigned int numTickedIn_{0}; /**< Number of samples written into the delay line*/
    unsigned int updateStepsLeft_{0};

    int lagrangeFilterOrder_{1}; /**< Lagrange filter order*/
    float halfLagrangeFilterOrder_{0.5f}; /**< Used in computed fractional delay sample*/
    std::vector<float> lagrangeFilterDenom_{std::vector<float>(2, 1.0)}; /**< Denominator of the lagrange filter coefficients (Pre-computed)*/
    std::vector<float> lagrangeFilter_{std::vector<float>(2, 0.0)}; /**< Lagrange filter coefficients*/

    Logger logger_{"DelayLine"};

    /*
     * Write one sample into the delay line
     *
     * @param   in  Input sample
     */
    void tickIn(float in);

    /*
     * Read one sample from the delay line
     *
     * @return  A single delayed sample
     */
    float tickOut();

    /*
     * Compute Lagrange filter coefficients
     *
     * @param   fracSamp    Fractional sample value for which Lagrange interpolation filter is to be computed
     */
    void updateLagrangeFilter(float fracSamp);
};

}; // namespace varays
#endif //__DELAYLINE__