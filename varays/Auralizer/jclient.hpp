//  -----------------------------------------------------------------------------
//
//  Copyright (C) 2006-2011 Fons Adriaensen <fons@linuxaudio.org>
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//  -----------------------------------------------------------------------------

#ifndef __JCLIENT_H
#define __JCLIENT_H

#include <map>
#include <memory>
#include <string>

#include <jack/jack.h>

#include "../Utils/Logger.hpp"
#include "./DopplerEngine/DopplerEngine.hpp"
#include "./prbsgen.hpp"
#include "./zita-convolver/zita-convolver.hpp"

using namespace varays;

class Jclient
{
  public:
    /**
     * Jack client
     *
     * @param   jname           Name of the Jack client
     * @param   jserv           Jack server to which the client is to be attached
     * @param   convproc        Pointer to the convolution processor
     * @param   dopplerEngine   Pointer to the Doppler engine
     */
    Jclient(const std::string& jname, const std::string& jserv, Convproc* convproc, DopplerEngine* dopplerEngine);
    ~Jclient(void);

    enum
    {
        FL_EXIT = 0x80000000,
        FL_BUFF = 0x40000000
    };

    /**
     * Return the name of the jack client
     */
    const std::string jname(void) const { return _jname; }

    /**
     * Get the sample rate of the jack session
     */
    unsigned int fsamp(void) const { return _fsamp; }

    /**
     * Get the jack buffer size
     */
    unsigned int fragm(void) const { return _fragm; }

    /**
     * Get the state of the convolution processor
     */
    unsigned int state(void) { return _convproc->state(); }

    unsigned int flags(void)
    {
        _clear = true;
        return _flags;
    }

    /**
     * Remove all input and output ports
     */
    int delete_ports(void);

    /**
     * Remove the given jack input port
     *
     * @param   name    Name of the input port
     * @return  0 if the input port is successfully removed, -1 otherwise
     */
    int delete_port(const std::string& name);

    /**
     * Add a new jack input port
     *
     * @param   name    Name of the input port
     * @param   conn    Jack port to connect the input port to
     */
    int add_input_port(const std::string& name, const std::string& conn = "");

    /**
     * Add a new jack output port
     *
     * @param   name    Name of the output port
     * @param   conn    Jack port to connect the output port to
     */
    int add_output_port(const std::string& name, const std::string& conn = "");

    void denorm_protect(bool v) { _addnoise = v; }

    /**
     * Check if an input port with the given name exists
     */
    bool is_input_port(const std::string& sourceName);

    /**
     * Start the convolver. This function is called after all the jack
     * ports are created and convolution resources have been created.
     */
    void start(void) { _convproc->start_process(_abspri, _policy); }

    /**
     * Stop the convolution processor. This is called before deleting ports and resources.
     */
    void stop(void) { _convproc->stop_process(); }

    /**
     * Check if initialization of the object is successful and ready to use.
     */
    bool isReady() { return _initSuccess; }

#ifdef __CONV_TEST__
    // These functions are used in tests to verify the operation of the convolver.
    jack_client_t* getJackClient() { return _jack_client; }
    void jackProcessCallback(float* inbuf, float* outbuf);
    void jack_process(float* inbuf, float* outbuf);
#endif

  private:
    /**
     * Initialize jack client
     *
     * @param   jname   Name of the jack client to be initialized
     * @param   jserv   Name of the jack server to be attached to
     * @return  true if initialization succeeds, false if it fails.
     */
    bool init_jack(const std::string& jname, const std::string& jserv);

    /**
     * Deactivate the jack client and remove it
     */
    void close_jack(void);
#ifndef __CONV_TEST__
    // This function is used during the normal operation of the jack client
    void jack_process(void);
#endif

    jack_client_t* _jack_client;
    std::map<std::string, jack_port_t*> _activeInputPorts;
    jack_port_t* _jack_outpp[Convproc::MAXOUT];
    std::string _jname;
    int _abspri;
    int _policy;
    unsigned int _fsamp;
    unsigned int _fragm;
    unsigned int _flags;
    bool _clear;
    unsigned int _ninp;
    unsigned int _nout;
    Convproc* _convproc;
    DopplerEngine* _dopplerEngine;
    bool _initSuccess; /** Initialization status. True if initialization succeeded, false otherwise. */
    bool _convsync;
    bool _addnoise;
    Prbsgen _prbsgen;

    Logger logger_{"Jclient"};

    static void jack_static_shutdown(void* arg);
    static void jack_static_freewheel(int state, void* arg);
    static int jack_static_buffsize(jack_nframes_t nframes, void* arg);
#ifndef __CONV_TEST__
    // This function is used during the normal operation of the jack client
    static int jack_static_process(jack_nframes_t nframes, void* arg);
#endif
};

#endif
