// ----------------------------------------------------------------------------
//
//  Copyright (C) 2006-2018 Fons Adriaensen <fons@linuxaudio.org>
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------

#include <algorithm>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "./zita-convolver.hpp"

int zita_convolver_major_version(void)
{
    return ZITA_CONVOLVER_MAJOR_VERSION;
}

int zita_convolver_minor_version(void)
{
    return ZITA_CONVOLVER_MINOR_VERSION;
}

float Convproc::_mac_cost = 1.0f;
float Convproc::_fft_cost = 5.0f;

static float* calloc_real(uint32_t k)
{
    float* p = fftwf_alloc_real(k);
    if (!p)
        throw(CONVERROR::MEM_ALLOC);
    memset(p, 0, k * sizeof(float));
    return p;
}

static fftwf_complex* calloc_complex(uint32_t k)
{
    fftwf_complex* p = fftwf_alloc_complex(k);
    if (!p)
        throw(CONVERROR::MEM_ALLOC);
    memset(p, 0, k * sizeof(fftwf_complex));
    return p;
}

Convproc::Convproc(void)
    : _state(ST_IDLE)
    , _options(0)
    , _skipcnt(0)
    , _ninp(0)
    , _nout(0)
    , _quantum(0)
    , _minpart(0)
    , _maxpart(0)
    , _nlevels(0)
    , _latecnt(0)
{
    memset(_outbuff, 0, MAXOUT * sizeof(float*));
    memset(_convlev, 0, MAXLEV * sizeof(Convlevel*));
}

Convproc::~Convproc(void)
{
    stop_process();
    cleanup();
}

void Convproc::set_options(uint32_t options)
{
    _options = options;
    for (size_t i = 0; i < _nlevels; ++i)
        _convlev[i]->_options = _options;
}

void Convproc::set_skipcnt(uint32_t skipcnt)
{
    if ((_quantum == _minpart) && (_quantum == _maxpart))
        _skipcnt = skipcnt;
}

float* Convproc::inpdata(const std::string& inp) const
{
    auto it = _inpbuff.find(inp);
    if (it != _inpbuff.end() && it->second.get())
        return &it->second.get()->operator[](_inpoffs);
    else
        return nullptr;
}

int Convproc::configure(uint32_t ninp, uint32_t nout, uint32_t maxsize, uint32_t quantum, uint32_t minpart, uint32_t maxpart, float density)
{
    uint32_t offs, npar, size, pind, nmin, i;
    int prio, step, d, r, s;
    float cfft, cmac;

    if (_state != ST_IDLE)
        return CONVERROR::BAD_STATE;
    if ((ninp < 1) || (ninp > MAXINP) || (nout < 1) || (nout > MAXOUT) || (quantum & (quantum - 1)) || (quantum < MINQUANT) || (quantum > MAXQUANT) ||
        (minpart & (minpart - 1)) || (minpart < MINPART) || (minpart < quantum) || (minpart > MAXDIVIS * quantum) || (maxpart & (maxpart - 1)) ||
        (maxpart > MAXPART) || (maxpart < minpart))
        return CONVERROR::BAD_PARAM;

    nmin = (ninp < nout) ? ninp : nout;
    if (density <= 0.0f)
        density = 1.0f / nmin;
    if (density > 1.0f)
        density = 1.0f;
    cfft = _fft_cost * (ninp + nout);
    cmac = _mac_cost * ninp * nout * density;
    step = (cfft < 4 * cmac) ? 1 : 2;
    if (step == 2)
    {
        r = maxpart / minpart;
        s = (r & 0xAAAA) ? 1 : 2;
    }
    else
        s = 1;
    nmin = (s == 1) ? 2 : 6;
    if (minpart == quantum)
        nmin++;
    prio = 0;
    size = quantum;
    while (size < minpart)
    {
        prio -= 1;
        size <<= 1;
    }

    try
    {
        for (offs = pind = 0; offs < maxsize; pind++)
        {
            npar = (maxsize - offs + size - 1) / size;
            if ((size < maxpart) && (npar > nmin))
            {
                r = 1 << s;
                d = npar - nmin;
                d = d - (d + r - 1) / r;
                if (cfft < d * cmac)
                    npar = nmin;
            }
            _convlev[pind] = new Convlevel();
            _convlev[pind]->configure(prio, offs, npar, size, _options);
            offs += size * npar;
            if (offs < maxsize)
            {
                prio -= s;
                size <<= s;
                s = step;
                nmin = (s == 1) ? 2 : 6;
            }
        }

        _ninp = ninp;
        _nout = nout;
        _quantum = quantum;
        _minpart = minpart;
        _maxpart = size;
        _nlevels = pind;
        _latecnt = 0;
        _inpsize = 2 * size;

        for (i = 0; i < nout; i++)
            _outbuff[i] = new float[_minpart];
    }
    catch (...)
    {
        cleanup();
        return CONVERROR::MEM_ALLOC;
    }

    _state = ST_STOP;
    return CONVERROR::NO_ERROR;
}

int Convproc::input_create(const std::string& inp)
{
    if (_inpbuff.size() >= _ninp)
        return CONVERROR::BAD_PARAM;
    auto it = _inpbuff.find(inp);
    if (it == _inpbuff.end())
    {
        auto newBuff = std::make_shared<std::vector<float>>(_inpsize, 0);
        _inpbuff.insert(std::make_pair(inp, newBuff));
        for (uint32_t j = 0; j < _nlevels; j++)
        {
            for (uint32_t k = 0; k < _nout; ++k)
                _convlev[j]->findmacnode(inp, k, true); // create input and output nodes
            _convlev[j]->setInpBuffer(inp, newBuff); // set sample buffer to input node
        }
    }
    return CONVERROR::NO_ERROR;
}

int Convproc::input_delete(const std::string& inp)
{
    auto it = _inpbuff.find(inp);
    if (it != _inpbuff.end())
    {
        for (uint32_t j = 0; j < _nlevels; ++j)
        {
            for (uint32_t k = 0; k < _nout; ++k)
                _convlev[j]->deleteinpnode(inp);
        }
        _inpbuff.erase(it);
    }
    return CONVERROR::NO_ERROR;
}

int Convproc::impdata_create(const std::string& inp, uint32_t out, int32_t step, float* data, int32_t ind0, int32_t ind1)
{
    uint32_t j;

    auto it = _inpbuff.find(inp);
    if ((it == _inpbuff.end()) || (out >= _nout))
        return CONVERROR::BAD_PARAM;

    // If a convolver doesn't exist for the given input and output, create one.
    if (it == _inpbuff.end())
    {
        auto newBuff = std::make_shared<std::vector<float>>(_inpsize, 0);
        memset(newBuff->data(), 0, _inpsize * sizeof(float));
        _inpbuff.insert(std::make_pair(inp, newBuff));
        for (j = 0; j < _nlevels; j++)
        {
            _convlev[j]->findmacnode(inp, out, true); // create input and output nodes
            _convlev[j]->setInpBuffer(inp, newBuff); // set sample buffer to input node
        }
    }
    try
    {
        for (j = 0; j < _nlevels; j++)
        {
            _convlev[j]->impdata_write(inp, out, step, data, ind0, ind1);
        }
    }
    catch (...)
    {
        cleanup();
        return CONVERROR::MEM_ALLOC;
    }
    return CONVERROR::NO_ERROR;
}

int Convproc::impdata_clear(const std::string& inp, uint32_t out)
{
    uint32_t k;

    if (_state < ST_STOP)
        return CONVERROR::BAD_STATE;
    for (k = 0; k < _nlevels; k++)
        _convlev[k]->impdata_clear(inp, out);
    return CONVERROR::NO_ERROR;
}

int Convproc::impdata_update(const std::string& inp, uint32_t out, int32_t step, float* data, int32_t ind0, int32_t ind1)
{
    uint32_t j;

    if (_state < ST_STOP)
        return CONVERROR::BAD_STATE;
    if ((_inpbuff.find(inp) == _inpbuff.end()) || (out >= _nout))
        return CONVERROR::BAD_PARAM;
    for (j = 0; j < _nlevels; j++)
    {
        _convlev[j]->impdata_write(inp, out, step, data, ind0, ind1);
    }
    return CONVERROR::NO_ERROR;
}

int Convproc::impdata_link(const std::string& inp1, uint32_t out1, const std::string& inp2, uint32_t out2)
{
    uint32_t j;

    if ((_inpbuff.find(inp1) == _inpbuff.end()) || (out1 >= _nout))
        return CONVERROR::BAD_PARAM;
    if ((_inpbuff.find(inp2) == _inpbuff.end()) || (out2 >= _nout))
        return CONVERROR::BAD_PARAM;
    if ((inp1 == inp2) && (out1 == out2))
        return CONVERROR::BAD_PARAM;
    if (_state != ST_STOP)
        return CONVERROR::BAD_STATE;
    try
    {
        for (j = 0; j < _nlevels; j++)
        {
            _convlev[j]->impdata_link(inp1, out1, inp2, out2);
        }
    }
    catch (...)
    {
        cleanup();
        return CONVERROR::MEM_ALLOC;
    }
    return CONVERROR::NO_ERROR;
}

int Convproc::reset(void)
{
    uint32_t k;

    if (_state == ST_IDLE)
        return CONVERROR::BAD_STATE;
    for (auto& it : _inpbuff)
        memset(it.second->data(), 0, _inpsize * sizeof(float));
    for (k = 0; k < _nout; k++)
        memset(_outbuff[k], 0, _minpart * sizeof(float));
    for (k = 0; k < _nlevels; k++)
        _convlev[k]->reset(_inpsize, _minpart, _outbuff);
    return CONVERROR::NO_ERROR;
}

int Convproc::start_process(int abspri, int policy)
{
    uint32_t k;

    if (_state != ST_STOP)
        return CONVERROR::BAD_STATE;
    _latecnt = 0;
    _inpoffs = 0;
    _outoffs = 0;
    reset();

    for (k = (_minpart == _quantum) ? 1 : 0; k < _nlevels; k++)
    {
        _convlev[k]->start(abspri, policy);
    }
    _state = ST_PROC;
    return CONVERROR::NO_ERROR;
}

int Convproc::process(bool sync)
{
    uint32_t k;
    int f = 0;

    if (_state != ST_PROC)
        return 0;
    _inpoffs += _quantum;
    if (_inpoffs == _inpsize)
        _inpoffs = 0;
    _outoffs += _quantum;
    if (_outoffs == _minpart)
    {
        _outoffs = 0;
        for (k = 0; k < _nout; k++)
            memset(_outbuff[k], 0, _minpart * sizeof(float));
        for (k = 0; k < _nlevels; k++)
            f |= _convlev[k]->readout(sync, _skipcnt);
        if (_skipcnt < _minpart)
            _skipcnt = 0;
        else
            _skipcnt -= _minpart;
        if (f)
        {
            if (++_latecnt >= 5)
            {
                if (~_options & OPT_LATE_CONTIN)
                    stop_process();
                f |= FL_LOAD;
            }
        }
        else
            _latecnt = 0;
    }
    return f;
}

int Convproc::stop_process(void)
{
    uint32_t k;

    if (_state != ST_PROC)
        return CONVERROR::BAD_STATE;
    for (k = 0; k < _nlevels; k++)
        _convlev[k]->stop();
    _state = ST_WAIT;
    return CONVERROR::NO_ERROR;
}

int Convproc::cleanup(void)
{
    uint32_t k;

    while (!check_stop())
    {
        usleep(100000);
    }
    for (auto& it : _inpbuff)
    {
        it.second.reset();
    }
    _inpbuff.clear();
    for (k = 0; k < _nout; k++)
    {
        delete[] _outbuff[k];
        _outbuff[k] = 0;
    }
    for (k = 0; k < _nlevels; k++)
    {
        delete _convlev[k];
        _convlev[k] = 0;
    }

    _state = ST_IDLE;
    _options = 0;
    _skipcnt = 0;
    _ninp = 0;
    _nout = 0;
    _quantum = 0;
    _minpart = 0;
    _maxpart = 0;
    _nlevels = 0;
    _latecnt = 0;
    return CONVERROR::NO_ERROR;
}

bool Convproc::check_stop(void)
{
    uint32_t k;

    for (k = 0; (k < _nlevels) && (_convlev[k]->_stat == Convlevel::ST_IDLE); k++)
        ;
    if (k == _nlevels)
    {
        _state = ST_STOP;
        return true;
    }
    return false;
}

void Convproc::print(FILE* F)
{
    uint32_t k;

    for (k = 0; k < _nlevels; k++)
        _convlev[k]->print(F);
}

typedef float FV4 __attribute__((vector_size(16)));

Convlevel::Convlevel(void)
    : _stat(ST_IDLE)
    , _npar(0)
    , _parsize(0)
    , _options(0)
    , _pthr(0)
    , _inp_list(0)
    , _out_list(0)
    , _plan_r2c(0)
    , _plan_c2r(0)
    , _time_data(0)
    , _prep_time_data(0)
    , _freq_data(0)
    , _prep_freq_data(0)
{
}

Convlevel::~Convlevel(void)
{
    cleanup();
}

void Convlevel::configure(int prio, uint32_t offs, uint32_t npar, uint32_t parsize, uint32_t options)
{
    int fftwopt = (options & OPT_FFTW_MEASURE) ? FFTW_MEASURE : FFTW_ESTIMATE;

    _prio = prio;
    _offs = offs;
    _npar = npar;
    _parsize = parsize;
    _parsize_inv = 1.0f / static_cast<float>(_parsize);
    _options = options;

    _fadeFunc = new float[_parsize];
    for (uint32_t i = 0; i < _parsize; ++i) // sine function for fading in
        _fadeFunc[i] = std::sin(i * M_PI_2 * _parsize_inv);

    _time_data = calloc_real(2 * _parsize);
    _prep_time_data = calloc_real(2 * _parsize);
    _freq_data = calloc_complex(_parsize + 1);
    _prep_freq_data = calloc_complex(_parsize + 1);
    _plan_r2c = fftwf_plan_dft_r2c_1d(2 * _parsize, _time_data, _freq_data, fftwopt);
    _plan_c2r = fftwf_plan_dft_c2r_1d(2 * _parsize, _freq_data, _time_data, fftwopt);
    if (_plan_r2c && _plan_c2r)
        return;
    throw(CONVERROR::MEM_ALLOC);
}

void Convlevel::impdata_write(const std::string& inp, uint32_t out, int32_t step, float* data, int32_t i0, int32_t i1)
{
    uint32_t k;
    int32_t j, j0, j1, n;
    float norm;
    fftwf_complex *fftb, *fftb_new;
    Macnode* M;

    n = i1 - i0;
    i0 = _offs - i0;
    i1 = i0 + _npar * _parsize;
    if (i1 <= 0)
        return;

    M = findmacnode(inp, out, false);
    if (M == 0 || M->_link || M->_fftb == 0)
        return;

    norm = 0.5f * _parsize_inv;
    for (k = 0; k < _npar; k++)
    {
        i1 = i0 + _parsize;
        if (((i0 < n) && (i1 > 0)) || !M->_swapInfo[k]._skipConv)
        {
            fftb = M->_fftb[k];
            fftb_new = M->_swapInfo[k]._fftb;
            if (fftb && fftb_new && data)
            {
                j0 = (i0 < 0) ? 0 : i0;
                j1 = (i1 > n) ? n : i1;
                if (i0 <= n)
                {
                    memset(_prep_time_data, 0, 2 * _parsize * sizeof(float));
                    for (j = j0; j < j1; j++)
                        _prep_time_data[j - i0] = norm * data[j * step];
                    fftwf_execute_dft_r2c(_plan_r2c, _prep_time_data, _prep_freq_data);
#ifdef ENABLE_VECTOR_MODE
                    if (_options & OPT_VECTOR_MODE)
                        fftswap(_prep_freq_data);
#endif
                    memcpy(fftb_new, _prep_freq_data, sizeof(fftwf_complex) * (_parsize + 1)); // completely replace the current fft partition
                    M->_swapInfo[k]._skipConv = false;
                }
                else
                {
                    if (M->_swapInfo[k]._skipConv == false)
                    {
                        memset(fftb_new, 0, (_parsize + 1) * sizeof(fftwf_complex));
                        M->_swapInfo[k]._convStop = true;
                    }
                }
                M->_swapInfo[k]._updatePart = true;
            }
        }
        i0 = i1;
    }
}

void Convlevel::impdata_clear(const std::string& inp, uint32_t out)
{
    uint32_t i;
    Macnode* M;

    M = findmacnode(inp, out, false);
    if (M == 0 || M->_link || M->_fftb == 0)
        return;
    for (i = 0; i < _npar; i++)
    {
        if (M->_fftb[i])
        {
            memset(M->_fftb[i], 0, (_parsize + 1) * sizeof(fftwf_complex));
        }
    }
}

void Convlevel::impdata_link(const std::string& inp1, uint32_t out1, const std::string& inp2, uint32_t out2)
{
    Macnode* M1;
    Macnode* M2;

    M1 = findmacnode(inp1, out1, false);
    if (!M1)
        return;
    M2 = findmacnode(inp2, out2, true);
    M2->free_fftb();
    M2->_link = M1;
}

void Convlevel::reset(uint32_t inpsize, uint32_t outsize, float** outbuff)
{
    uint32_t i;
    Inpnode* X;
    Outnode* Y;

    _inpsize = inpsize;
    _outsize = outsize;
    _outbuff = outbuff;
    for (X = _inp_list; X; X = X->_next)
    {
        for (i = 0; i < _npar; i++)
        {
            memset(X->_ffta[0][i], 0, (_parsize + 1) * sizeof(fftwf_complex));
            memset(X->_ffta[1][i], 0, (_parsize + 1) * sizeof(fftwf_complex));
        }
    }
    for (Y = _out_list; Y; Y = Y->_next)
    {
        for (i = 0; i < 3; i++)
        {
            memset(Y->_buff[i], 0, _parsize * sizeof(float));
        }
    }
    if (_parsize == _outsize)
    {
        _outoffs = 0;
        _inpoffs = 0;
    }
    else
    {
        _outoffs = _parsize / 2;
        _inpoffs = _inpsize - _outoffs;
    }
    _bits = _parsize / _outsize;
    _wait = 0;
    _ptind = 0;
    _opind = 0;
    _trig.init(0, 0);
    _done.init(0, 0);
    _proc.init(0, 1);
}

void Convlevel::setInpBuffer(const std::string& inp, std::shared_ptr<std::vector<float>> buff)
{
    Inpnode* X;
    for (X = _inp_list; X && (X->_inp != inp); X = X->_next)
        ;
    if (X)
    {
        _proc.wait();
        X->_inpbuff = std::move(buff);
        _proc.post();
    }
}

void Convlevel::start(int abspri, int policy)
{
    int min, max;
    pthread_attr_t attr;
    struct sched_param parm;

    _pthr = 0;
    min = sched_get_priority_min(policy);
    max = sched_get_priority_max(policy);
    abspri += _prio;
    if (abspri > max)
        abspri = max;
    if (abspri < min)
        abspri = min;
    parm.sched_priority = abspri;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_attr_setschedpolicy(&attr, policy);
    pthread_attr_setschedparam(&attr, &parm);
    pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);
    pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setstacksize(&attr, 0x10000);
    pthread_create(&_pthr, &attr, static_main, this);
    pthread_attr_destroy(&attr);
}

void Convlevel::stop(void)
{
    if (_pthr) // Signal process termination if it is running on a separate thread
    {
        _stat = ST_TERM;
        _trig.post();
    }
}

void Convlevel::cleanup(void)
{
    Inpnode *X, *X1;
    Outnode *Y, *Y1;
    Macnode *M, *M1;

    X = _inp_list;
    while (X)
    {
        X1 = X->_next;
        delete X;
        X = X1;
    }
    _inp_list = 0;

    Y = _out_list;
    while (Y)
    {
        M = Y->_list;
        while (M)
        {
            M1 = M->_next;
            delete M;
            M = M1;
        }
        Y1 = Y->_next;
        delete Y;
        Y = Y1;
    }
    _out_list = 0;

    fftwf_destroy_plan(_plan_r2c);
    fftwf_destroy_plan(_plan_c2r);
    fftwf_free(_time_data);
    fftwf_free(_prep_time_data);
    fftwf_free(_freq_data);
    fftwf_free(_prep_freq_data);
    delete[] _fadeFunc;
    _plan_r2c = 0;
    _plan_c2r = 0;
    _time_data = 0;
    _prep_time_data = 0;
    _freq_data = 0;
    _prep_freq_data = 0;
}

void* Convlevel::static_main(void* arg)
{
    ((Convlevel*)arg)->main();
    return 0;
}

void Convlevel::main(void)
{
    if (_stat == ST_IDLE)
        _stat = ST_PROC;
    while (_stat == ST_PROC)
    {
        _trig.wait();
        process(false);
        _done.post();
    }
    if (_stat == ST_TERM)
    {
        _stat = ST_IDLE;
        _pthr = 0;
    }
}

void Convlevel::process(bool skip)
{
    uint32_t i, i1, j, k, n1, n2, opi1, opi2;
    Inpnode* X;
    Macnode* M;
    Outnode* Y;
    fftwf_complex *ffta_fIn, *ffta_fOut;
    fftwf_complex *fftb, *fftb_new;
    float* outd;

    i1 = _inpoffs;
    n1 = _parsize;
    n2 = 0;
    _inpoffs = i1 + n1;
    if (_inpoffs >= _inpsize)
    {
        _inpoffs -= _inpsize;
        n2 = _inpoffs;
        n1 -= n2;
    }

    opi1 = (_opind + 1) % 3;
    opi2 = (_opind + 2) % 3;

    _proc.wait();
    for (X = _inp_list; X; X = X->_next)
    {
        if (!X->_inpbuff.get())
            continue;
        float* inpd = &X->_inpbuff.get()->operator[](0);
        if (n1)
            memcpy(_time_data, inpd + i1, n1 * sizeof(float));
        if (n2)
            memcpy(_time_data + n1, inpd, n2 * sizeof(float));
        memset(_time_data + _parsize, 0, _parsize * sizeof(float));
        for (k = 0; k < _parsize; ++k)
            _time_data[k] *= _fadeFunc[k]; // fade in
        fftwf_execute_dft_r2c(_plan_r2c, _time_data, X->_ffta[0][_ptind]);

        if (n1)
            memcpy(_time_data, inpd + i1, n1 * sizeof(float));
        if (n2)
            memcpy(_time_data + n1, inpd, n2 * sizeof(float));
        memset(_time_data + _parsize, 0, _parsize * sizeof(float));
        for (k = 0; k < _parsize; ++k)
            _time_data[k] *= 1.0f - _fadeFunc[k]; // fade out
        fftwf_execute_dft_r2c(_plan_r2c, _time_data, X->_ffta[1][_ptind]);

#ifdef ENABLE_VECTOR_MODE
        if (_options & OPT_VECTOR_MODE)
        {
            fftswap(X->_ffta[0][_ptind]);
            fftswap(X->_ffta[1][_ptind]);
        }
#endif
    }

    if (skip)
    {
        for (Y = _out_list; Y; Y = Y->_next)
        {
            outd = Y->_buff[opi2];
            memset(outd, 0, _parsize * sizeof(float));
        }
    }
    else
    {
        for (Y = _out_list; Y; Y = Y->_next)
        {
            memset(_freq_data, 0, (_parsize + 1) * sizeof(fftwf_complex));
            for (M = Y->_list; M; M = M->_next)
            {
                X = M->_inpn;
                i = _ptind;
                for (j = 0; j < _npar; j++)
                {
                    ffta_fIn = X->_ffta[0][i];
                    ffta_fOut = X->_ffta[1][i];
                    fftb = M->_link ? M->_link->_fftb[j] : M->_fftb[j];
                    fftb_new = M->_link ? M->_link->_swapInfo[j]._fftb : M->_swapInfo[j]._fftb;
                    if (fftb && fftb_new && !M->_swapInfo[j]._skipConv)
                    {
                        if (M->_swapInfo[j]._updatePart)
                        {
#ifdef ENABLE_VECTOR_MODE
                            if (_options & OPT_VECTOR_MODE)
                            {
                                FV4* I0 = (FV4*)ffta_fOut;
                                FV4* I1 = (FV4*)ffta_fIn;
                                FV4* H0 = (FV4*)fftb;
                                FV4* H1 = (FV4*)fftb_new;
                                FV4* O = (FV4*)_freq_data;
                                for (k = 0; k < _parsize; k += 4)
                                {
                                    O[0] += I0[0] * H0[0] - I0[1] * H0[1] + I1[0] * H1[0] - I1[1] * H1[1];
                                    O[1] += I0[0] * H0[1] + I0[1] * H0[0] + I1[0] * H1[1] + I1[1] * H1[0];
                                    I0 += 2;
                                    I1 += 2;
                                    H0 += 2;
                                    H1 += 2;
                                    O += 2;
                                }
                                _freq_data[_parsize][0] += ffta_fOut[_parsize][0] * fftb[_parsize][0] + ffta_fIn[_parsize][0] * fftb_new[_parsize][0];
                                _freq_data[_parsize][1] = 0;
                            }
                            else
#endif
                            {
                                for (k = 0; k <= _parsize; ++k)
                                {
                                    _freq_data[k][0] += ffta_fIn[k][0] * fftb_new[k][0] - ffta_fIn[k][1] * fftb_new[k][1];
                                    _freq_data[k][1] += ffta_fIn[k][0] * fftb_new[k][1] + ffta_fIn[k][1] * fftb_new[k][0];
                                    _freq_data[k][0] += ffta_fOut[k][0] * fftb[k][0] - ffta_fOut[k][1] * fftb[k][1];
                                    _freq_data[k][1] += ffta_fOut[k][0] * fftb[k][1] + ffta_fOut[k][1] * fftb[k][0];
                                }
                            }
                            std::swap(M->_fftb[j], M->_swapInfo[j]._fftb);
                            M->_swapInfo[j]._updatePart = false;
                            if (M->_swapInfo[j]._convStop)
                            {
                                M->_swapInfo[j]._convStop = false;
                                M->_swapInfo[j]._skipConv = true;
                            }
                        }
                        else
                        {
#ifdef ENABLE_VECTOR_MODE
                            if (_options & OPT_VECTOR_MODE)
                            {
                                FV4* I0 = (FV4*)ffta_fOut;
                                FV4* I1 = (FV4*)ffta_fIn;
                                FV4* H = (FV4*)fftb;
                                FV4* O = (FV4*)_freq_data;
                                for (k = 0; k < _parsize; k += 4)
                                {
                                    FV4 S0 = I0[0] + I1[0];
                                    FV4 S1 = I0[1] + I1[1];
                                    O[0] += S0 * H[0] - S1 * H[1];
                                    O[1] += S0 * H[1] + S1 * H[0];
                                    I0 += 2;
                                    I1 += 2;
                                    H += 2;
                                    O += 2;
                                }
                                _freq_data[_parsize][0] += (ffta_fOut[_parsize][0] + ffta_fIn[_parsize][0]) * fftb[_parsize][0];
                                _freq_data[_parsize][1] = 0;
                            }
                            else
#endif
                            {
                                for (k = 0; k <= _parsize; ++k)
                                {
                                    fftwf_complex sum = {ffta_fIn[k][0] + ffta_fOut[k][0], ffta_fIn[k][1] + ffta_fOut[k][1]};
                                    _freq_data[k][0] += sum[0] * fftb[k][0] - sum[1] * fftb[k][1];
                                    _freq_data[k][1] += sum[0] * fftb[k][1] + sum[1] * fftb[k][0];
                                }
                            }
                        }
                    }
                    if (i == 0)
                        i = _npar;
                    i--;
                }
            }

#ifdef ENABLE_VECTOR_MODE
            if (_options & OPT_VECTOR_MODE)
                fftswap(_freq_data);
#endif
            fftwf_execute_dft_c2r(_plan_c2r, _freq_data, _time_data);
            outd = Y->_buff[opi1];
            for (k = 0; k < _parsize; k++)
                outd[k] += _time_data[k];
            outd = Y->_buff[opi2];
            memcpy(outd, _time_data + _parsize, _parsize * sizeof(float));
        }
    }

    _ptind++;
    if (_ptind == _npar)
        _ptind = 0;
    _proc.post();
}

int Convlevel::readout(bool sync, uint32_t skipcnt)
{
    uint32_t i;
    float *p, *q;
    Outnode* Y;

    _outoffs += _outsize;
    if (_outoffs == _parsize)
    {
        _outoffs = 0;
        if (_stat == ST_PROC)
        {
            while (_wait)
            {
                if (sync)
                    _done.wait();
                else if (_done.trywait())
                    break;
                _wait--;
            }
            if (++_opind == 3)
                _opind = 0;
            _trig.post();
            _wait++;
        }
        else
        {
            process(skipcnt >= 2 * _parsize);
            if (++_opind == 3)
                _opind = 0;
        }
    }

    for (Y = _out_list; Y; Y = Y->_next)
    {
        p = Y->_buff[_opind] + _outoffs;
        q = _outbuff[Y->_out];
        for (i = 0; i < _outsize; i++)
            q[i] += p[i];
    }

    return (_wait > 1) ? _bits : 0;
}

void Convlevel::print(FILE* F)
{
    fprintf(F, "prio = %4d, offs = %6d,  parsize = %5d,  npar = %3d\n", _prio, _offs, _parsize, _npar);
}

Macnode* Convlevel::findmacnode(const std::string& inp, uint32_t out, bool create)
{
    Inpnode* X;
    Outnode* Y;
    Macnode* M;

    for (X = _inp_list; X && (X->_inp != inp); X = X->_next)
        ;
    if (!X)
    {
        if (!create)
            return 0;
        X = new Inpnode(inp);
        X->_next = _inp_list;
        X->alloc_ffta(_npar, _parsize);
        _inp_list = X;
    }

    for (Y = _out_list; Y && (Y->_out != out); Y = Y->_next)
        ;
    if (!Y)
    {
        if (!create)
            return 0;
        Y = new Outnode(out, _parsize);
        Y->_next = _out_list;
        _out_list = Y;
    }

    for (M = Y->_list; M && (M->_inpn != X); M = M->_next)
        ;
    if (!M)
    {
        if (!create)
            return 0;
        M = new Macnode(X);
        M->alloc_fftb(_npar);
        for (uint32_t k = 0; k < _npar; k++)
        {
            M->_fftb[k] = calloc_complex(_parsize + 1);
            M->_swapInfo[k]._fftb = calloc_complex(_parsize + 1);
        }
        M->_next = Y->_list;
        Y->_list = M;
    }
    return M;
}

void Convlevel::deleteinpnode(const std::string& inp)
{
    Inpnode *X, *prevX = nullptr;
    Outnode* Y;
    Macnode* M;

    // Find the Inpnode corresponding to the given input
    for (X = _inp_list; X && (X->_inp != inp); X = X->_next)
        prevX = X;
    if (X)
    {
        // Delete all Macnodes associated with the Inpnode
        _proc.wait();
        for (Y = _out_list; Y; Y = Y->_next)
        {
            Macnode* prevM = nullptr;
            for (M = Y->_list; M && (M->_inpn != X); M = M->_next)
                prevM = M;
            if (M)
            {
                if (prevM)
                    prevM->_next = M->_next;
                else
                    Y->_list = M->_next;
                delete M;
            }
        }
        if (prevX)
            prevX->_next = X->_next;
        else
            _inp_list = X->_next;
        delete X;
        _proc.post();
    }
}

#ifdef ENABLE_VECTOR_MODE

void Convlevel::fftswap(fftwf_complex* p)
{
    uint32_t n = _parsize;
    float a, b;

    while (n)
    {
        a = p[2][0];
        b = p[3][0];
        p[2][0] = p[0][1];
        p[3][0] = p[1][1];
        p[0][1] = a;
        p[1][1] = b;
        p += 4;
        n -= 4;
    }
}

#endif

Inpnode::Inpnode(const std::string& inp)
    : _next(0)
    , _ffta{0, 0}
    , _npar(0)
    , _inp(inp)
{
}

Inpnode::~Inpnode(void)
{
    free_ffta();
}

void Inpnode::alloc_ffta(uint16_t npar, int32_t size)
{
    _npar = npar;
    _ffta[0] = new fftwf_complex*[_npar];
    _ffta[1] = new fftwf_complex*[_npar];
    for (int i = 0; i < _npar; i++)
    {
        _ffta[0][i] = calloc_complex(size + 1);
        _ffta[1][i] = calloc_complex(size + 1);
    }
}

void Inpnode::free_ffta(void)
{
    if (!_ffta)
        return;
    for (uint16_t i = 0; i < _npar; i++)
    {
        fftwf_free(_ffta[0][i]);
        fftwf_free(_ffta[1][i]);
    }
    delete[] _ffta[0];
    delete[] _ffta[1];
    _ffta[0] = 0;
    _ffta[1] = 0;
    _npar = 0;
}

Macnode::Macnode(Inpnode* inpn)
    : _next(0)
    , _inpn(inpn)
    , _link(0)
    , _fftb(0)
    , _npar(0)
    , _swapInfo(0)
{
}

Macnode::~Macnode(void)
{
    free_fftb();
}

void Macnode::alloc_fftb(uint16_t npar)
{
    _npar = npar;
    _fftb = new fftwf_complex*[_npar];
    _swapInfo = new PartitionSwap[_npar];
    for (uint16_t i = 0; i < _npar; i++)
    {
        _fftb[i] = 0;
    }
}

void Macnode::free_fftb(void)
{
    if (!_fftb)
        return;
    for (uint16_t i = 0; i < _npar; i++)
    {
        fftwf_free(_fftb[i]);
        fftwf_free(_swapInfo[i]._fftb);
    }
    delete[] _fftb;
    delete[] _swapInfo;
    _fftb = 0;
    _npar = 0;
    _swapInfo = 0;
}

Outnode::Outnode(uint16_t out, int32_t size)
    : _next(0)
    , _list(0)
    , _out(out)
{
    _buff[0] = calloc_real(size);
    _buff[1] = calloc_real(size);
    _buff[2] = calloc_real(size);
}

Outnode::~Outnode(void)
{
    fftwf_free(_buff[0]);
    fftwf_free(_buff[1]);
    fftwf_free(_buff[2]);
}
