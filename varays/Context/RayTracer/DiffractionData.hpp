// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <Eigen/Dense>

#include "../Scene/GeomData.hpp"

#ifndef __VARAYS_DIFFDATA__
#define __VARAYS_DIFFDATA__

namespace varays
{

class DiffractionData : public GeomData
{
  public:
    DiffractionData(int nbVertices, AudioMat* mat, unsigned int id, unsigned int volID, Eigen::Vector3f position1, Eigen::Vector3f position2, float size)
        : GeomData(nbVertices, mat)
        , id_(id)
        , volID_(volID)
        , position1_(position1)
        , position2_(position2)
        , size_(size){};

    virtual int getNbVertices() const { return nbVertices_; }

    unsigned int getID() const { return id_; }
    unsigned int getvolID() const { return volID_; }
    Eigen::Vector3f getPosition1() const { return position1_; }
    Eigen::Vector3f getPosition2() const { return position2_; }
    float getSize() const { return size_; }

  private:
    unsigned int id_;
    unsigned int volID_;
    Eigen::Vector3f position1_;
    Eigen::Vector3f position2_;
    float size_;
};
}; // namespace varays

#endif
