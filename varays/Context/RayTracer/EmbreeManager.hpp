// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef __VARAYS_EMBREE_MANAGER__
#define __VARAYS_EMBREE_MANAGER__

#include <embree3/rtcore.h>
#include <future>
#include <vector>

#include "./utils.hpp"

namespace varays
{
class EmbreeManager
{
  public:
    /**
    * The EmbreeManager is the only class which has access to Embree. 
    * It can both throw a set of rays in a specified scene or check for the visibility of a set of rays in a specified scene.
    * As the embree library can use many cores, only one EmbreeManager should exist at the same time.
    */
  
    /**
    * Throw rays in a secene, and returns their collision points as well as information about the surface hit
    * 
    * @param originsAndDirections Origins, directions and energies of rays to be thrown
    * @param scene Scene in which the rays are thrown
    * @return Returns a vector of NodeInfo, which contains the collision point of a ray as well as information about the surface hit
    */
    std::vector<utils::NodeInfo> ThrowRays(const std::vector<utils::RayToThrow>& originsAndDirections, RTCScene& scene);
    
    /**
    * Checks whether sources are visible or not in a specific scene for a quantity of positions
    * 
    * @param sources Sources we want to test the visibility of points for
    * @param positions ...
    * @param scene Scene in which the rays are thrown
    * @return Returns visibility results for sources and positions
    */
    std::vector<std::vector<bool>> VisibilityTests(const std::vector<AudioSource>& sources, const std::vector<Eigen::Vector3f>& positions, RTCScene& scene);

  private:
    int throwByteStride_{sizeof(RTCRayHit)};
    int visibilityByteStride_{sizeof(RTCRay)};
};
}; // namespace varays
#endif // __VARAYS_EMBREE_MANAGER__