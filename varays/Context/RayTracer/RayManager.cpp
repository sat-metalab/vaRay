// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "./RayManager.hpp"
#include "./utils.hpp"

namespace varays
{
RayManager::RayManager(int minReflectionOrder, int maxReflectionOrder, float targetFrequency, float terminationEnergy, varays::EmbreeManager* embreeManager)
    : minReflectionOrder_(minReflectionOrder)
    , maxReflectionOrder_(maxReflectionOrder)
    , targetFrequency_(targetFrequency)
    , terminationEnergy_(terminationEnergy)
    , embreeManager_(embreeManager)
{
}

// Generates the soundevents for a full generation of rays
std::vector<SoundEvent> RayManager::GetGeneration(const Eigen::Vector3f& listenerCoordinates, const std::vector<AudioSource>& sources, int initRays, int orderBreakpoint, int nReflections, RTCScene& scene)
{
    ResetRayTree(listenerCoordinates, sources);
    // first order
    std::vector<SoundEvent> soundEvents;
    if (minReflectionOrder_ == 0)
        soundEvents = ComputeFirstOrder(initRays, sources, scene);
    else
        ComputeFirstOrder(initRays, sources, scene);

    // every subsequent order
    for (int i = 2; i <= maxReflectionOrder_; ++i)
    {
        std::vector<SoundEvent> newSoundEvents;
        if (i<orderBreakpoint)
            newSoundEvents = ComputeReflectionOrder(i, nReflections, sources, scene);
        else
            newSoundEvents = ComputeReflectionOrder(i, 1, sources, scene);
        if (minReflectionOrder_ <= i)
            soundEvents.insert(soundEvents.end(), newSoundEvents.begin(), newSoundEvents.end());
    }
    return soundEvents;
}

// Computes direct soundEvents. Can be called from the context directly if necessary.
std::vector<SoundEvent> RayManager::ComputeDirectSounds(const std::vector<AudioSource>& sources, RTCScene& scene, const Eigen::Vector3f& coordinates)
{
    std::vector<SoundEvent> directSoundEvents;
    auto directVisilibity = SendVisibilityTests(sources, std::vector<Eigen::Vector3f>{coordinates}, scene);
    for (size_t i = 0; i < directVisilibity.size(); ++i)
    {
        // If the source is visible, create a soundevent for that source
        if (directVisilibity[0][i])
        {
            // Calculate the proportion of initial energy that reaches the source
            float energyProportion = utils::visibilityAngleProportion(sources[i].getRadius(), coordinates, sources[i].getPos());
            varays::energy energyMap = sources[i].getEnergy();
            std::vector<float> frequencies, energies;
            for (auto it = energyMap.begin(); it != energyMap.end(); ++it)
            {
                frequencies.push_back(it->first);
                energies.push_back(it->second * energyProportion);
            }
            Eigen::Vector3f trajectory = sources[i].getPos() - coordinates;
            SoundEvent newSoundEvent = SoundEvent(SoundEvent::SoundEventConfig(sources[i].getId(), // source ID
                trajectory.norm(), // Length
                frequencies, // Frequencies
                energies, // Energies
                utils::reverseSphericalVector(Eigen::Vector3f(1.f, atan2(trajectory[1], trajectory[0]), acos(trajectory[2]))),
                std::vector<SoundEvent::Reflection>{})); // reflection types
            rayTree_.soundEvents.emplace_back(newSoundEvent);
            directSoundEvents.emplace_back(newSoundEvent);
        }
    }
    return directSoundEvents;
}

// 1st order corresponds to the initial rays thrown from the listener
std::vector<SoundEvent> RayManager::ComputeFirstOrder(int initRays, const std::vector<AudioSource>& sources, RTCScene& scene)
{
    std::vector<SoundEvent> firstOrderSoundEvents = ComputeDirectSounds(sources, scene, rayTree_.coordinates);
    rayTree_.soundEvents = firstOrderSoundEvents;

    // Send first generation of rays
    std::vector<utils::RayToThrow> raysToThrow;
    for (int i = 0; i < initRays; ++i)
    {
        raysToThrow.push_back(utils::RayToThrow{.origin = rayTree_.coordinates, .direction = utils::sphereRandom(), .weights = utils::SpecAndDiff{.specular = 1.f / static_cast<float>(initRays), .diffuse = 0.f}});
    }
    // ComputeRayGeneration
    auto newNodesInfo = SendNewRays(raysToThrow, scene);
    std::vector<TreeNode> newNodes;

    std::map<int, varays::energy> initRayEnergies = utils::initialRayEnergy(initRays, rayTree_.energies);
    for (size_t i = 0; i < raysToThrow.size(); ++i)
    {
        if (newNodesInfo[i].type != utils::NodeType::INVALID)
            newNodes.emplace_back(utils::makeNode(newNodesInfo[i], raysToThrow[i].origin, initRayEnergies, 0.f, {}));
    }
    // ComputeVisibility
    std::vector<Eigen::Vector3f> newNodesPositions;
    for (const auto& node : newNodes)
    {
        newNodesPositions.emplace_back(node.coordinates);
    }
    auto visibilityResults = SendVisibilityTests(sources, newNodesPositions, scene);
    for (size_t i = 0; i < newNodes.size(); ++i)
    {
        utils::computeSoundEvents(visibilityResults[i], sources, newNodes[i]);
    }

    // fill soundevents vector with the new soundevents
    for (const auto& node : newNodes)
    {
        firstOrderSoundEvents.insert(firstOrderSoundEvents.end(), node.soundEvents.begin(), node.soundEvents.end());
    }

    // update the rayTree
    rayTree_.children.insert(rayTree_.children.end(), newNodes.begin(), newNodes.end());

    return firstOrderSoundEvents;
}

std::vector<SoundEvent> RayManager::ComputeReflectionOrder(int order, int reflections, const std::vector<AudioSource>& sources, RTCScene& scene)
{
    // Find nodes that correspond to the right generation
    std::vector<TreeNode*> endNodes = rayTree_.findEndNodes(order - 1);
    // Send the new rays
    // Determine new directions and energy repartitions
    std::vector<utils::RayToThrow> raysToThrow;

    for (uint32_t i = 0; i < endNodes.size(); ++i)
    {
        std::vector<utils::RayToThrow> newRaysToThrow =
            utils::reflectionDirections(endNodes[i]->coordinates, endNodes[i]->rayDirection, endNodes[i]->normal, reflections);
        raysToThrow.insert(raysToThrow.end(), newRaysToThrow.begin(), newRaysToThrow.end());
    }

    assert(raysToThrow.size() == reflections * endNodes.size());

    // ComputeRayGeneration
    auto newNodesInfo = SendNewRays(raysToThrow, scene);

    std::vector<TreeNode> newNodes;
    for (size_t i = 0; i < raysToThrow.size(); ++i)
    {
        if (newNodesInfo[i].type != utils::NodeType::INVALID)
        {
            std::vector<SoundEvent::Reflection> childReflectionTypes = endNodes[i / reflections]->reflectionTypes;
            if (i % reflections == 0)
                childReflectionTypes.emplace_back(SoundEvent::Reflection::SPECULAR);
            else
                childReflectionTypes.emplace_back(SoundEvent::Reflection::DIFFUSION);

            newNodes.emplace_back(utils::makeNode(newNodesInfo[i],
                raysToThrow[i].origin,
                utils::reflectedRayEnergy(raysToThrow[i], endNodes[i / reflections]->outgoingEnergies),
                endNodes[i / reflections]->length,
                childReflectionTypes));
        }
    }

    // ComputeVisibility
    std::vector<Eigen::Vector3f> newNodesPositions;
    for (const auto& node : newNodes)
    {
        newNodesPositions.emplace_back(node.coordinates);
    }
    auto visibilityResults = SendVisibilityTests(sources, newNodesPositions, scene);

    for (size_t i = 0; i < newNodes.size(); ++i)
    {
        utils::computeSoundEvents(visibilityResults[i], sources, newNodes[i]);
    }

    size_t j = 0;
    for (size_t i = 0; i < newNodesInfo.size(); ++i)
    {
        if (newNodesInfo[i].type != utils::NodeType::INVALID)
        {
            endNodes[i / reflections]->children.emplace_back(newNodes[j]);
            ++j;
        }
    }

    std::vector<SoundEvent> soundEvents;
    for (const auto& node : newNodes)
    {
        soundEvents.insert(soundEvents.end(), node.soundEvents.begin(), node.soundEvents.end());
    }
    return soundEvents;
}

std::vector<utils::NodeInfo> RayManager::SendNewRays(
    const std::vector<utils::RayToThrow>& originsAndDirections, RTCScene& scene)
{
    return embreeManager_->ThrowRays(originsAndDirections, scene);
}

std::vector<std::vector<bool>> RayManager::SendVisibilityTests(const std::vector<AudioSource>& sources,
    const std::vector<Eigen::Vector3f>& positions,
    RTCScene& scene)
{
    return embreeManager_->VisibilityTests(sources, positions, scene);
}

void RayManager::ResetRayTree(const Eigen::Vector3f& newCoordinates, const std::vector<AudioSource>& sources)
{
    rayTree_ = RootNode(newCoordinates, sources);
}
}; // namespace varays