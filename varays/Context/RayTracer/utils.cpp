// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <algorithm>
#include <cmath>
#include <random>

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include "./utils.hpp"
#include "../../Utils/Timer.hpp"

namespace varays
{
// The first ray returned will always be specular. Every other ray will be diffuse.
// The energy each outgoing ray carries will be calculated according to the different attenuation factors
std::vector<utils::RayToThrow> utils::reflectionDirections(const Eigen::Vector3f& origin, const Eigen::Vector3f& raydir, const Eigen::Vector3f& normal, int nbRay)
{
    assert(nbRay > 0);

    std::vector<utils::RayToThrow> outgoingRays;

    // Calculate specular reflection from incoming direction and normal
    Eigen::Vector3f specularDirection = (raydir - 2.0 * normal.dot(raydir) * normal).normalized();

    // return only specular
    if (nbRay == 1)
    {
        outgoingRays.emplace_back(utils::RayToThrow{.origin = origin, .direction = specularDirection, .weights = SpecAndDiff{.specular = 1.f, .diffuse = 1.f}});
    }
    else
    {
        // specular ray
        outgoingRays.emplace_back(utils::RayToThrow{.origin = origin, .direction = specularDirection, .weights = SpecAndDiff{.specular = 1.f, .diffuse = 0.f}});

        // diffuse rays
        for (int i = 0; i < nbRay - 1; ++i)
        {
            // lambertian distribution (ball random)
            Eigen::Vector3f diffuseDirection = ballRandom(); // Random in a ball (from -1 to 1 for every dimension)
            // Calculate the directional outgoing vector
            diffuseDirection = (diffuseDirection + normal).normalized();
            outgoingRays.emplace_back(utils::RayToThrow{.origin = origin, .direction = diffuseDirection, .weights = SpecAndDiff{.specular = 1.f, .diffuse = 1.f / static_cast<float>(nbRay)}});
        }
    }
    return outgoingRays;
}

Eigen::Vector3f utils::reverseSphericalVector(Eigen::Vector3f sphericalVector)
{
    if (sphericalVector[1] > 0)
        sphericalVector[1] -= M_PI;
    else
        sphericalVector[1] += M_PI;

    return sphericalVector;
}

// Returns an equally distributed random vector3f within a sphere of radius 1
Eigen::Vector3f utils::ballRandom()
{
    Eigen::Vector3f randVec;
    while(true)
    {
        randVec = Eigen::Vector3f::Random();
        if (randVec.norm() <= 1.0)
            break;
    }
    return randVec;
}

// Returns an equally distributed random vector3f on the surface of a sphere of radius 1#
Eigen::Vector3f utils::sphereRandom()
{
    return ballRandom().normalized();
}

std::map<int, varays::energy> utils::initialRayEnergy(int raysToThrow, const std::map<int, varays::energy>& parentEnergies)
{
    // Calculate incoming energies
    std::map<int, varays::energy> incomingEnergies = parentEnergies;
    for (auto incomingEnergiesIt = incomingEnergies.begin(); incomingEnergiesIt != incomingEnergies.end(); ++incomingEnergiesIt)
    {
        for (auto energyIt = incomingEnergiesIt->second.begin(); energyIt != incomingEnergiesIt->second.end(); ++energyIt)
            energyIt->second /= static_cast<float>(raysToThrow);
    }

    return incomingEnergies;
}

std::map<int, varays::energy> utils::reflectedRayEnergy(const RayToThrow& raySent, const std::map<int, Energies>& parentEnergies)
{
    // Calculate incoming energies
    std::map<int, varays::energy> incomingEnergies;
    for (auto& [parentSourceID, parentEnergy] : parentEnergies)
    {
        varays::energy incomingEnergy;
        // for specular energy
        for (auto& [frequency, energy] : parentEnergy.specular)
            incomingEnergy[frequency] = energy * raySent.weights.specular;
        // for diffuse energy
        for (auto& [frequency, energy] : parentEnergy.diffuse)
            incomingEnergy.find(frequency)->second += energy * raySent.weights.diffuse;

        incomingEnergies[parentSourceID] = incomingEnergy;
    }
    return incomingEnergies;
}

TreeNode utils::makeNode(NodeInfo& newNodeInfo,
    const Eigen::Vector3f& origin,
    const std::map<int, varays::energy>& incomingEnergies,
    float parentLength,
    const std::vector<SoundEvent::Reflection>& reflectionTypes)
{
    assert(newNodeInfo.type_ != NodeType::INVALID);
    Eigen::Vector3f rayVector = newNodeInfo.coordinates - origin;
    Eigen::Vector3f normalizedDir = rayVector.normalized();

    std::map<int, Energies> outgoingEnergies;
    // For each source
    for (auto& [sourceID, globalEnergy] : incomingEnergies)
    {
        varays::energy specEnergy, diffEnergy;
        for (auto& [frequency, energyForFreq] : globalEnergy)
        {
            // Reflected energy (specular + diffuse) for the frequency it->first
            float energy = (1.f - (newNodeInfo.material.getAbsorption(frequency) + newNodeInfo.material.getTransmission(frequency))) * energyForFreq;
            float scattering = newNodeInfo.material.getScattering(frequency);
            specEnergy[frequency] = energy * (1 - scattering);
            diffEnergy[frequency] = energy * scattering;   
        }
        outgoingEnergies[sourceID] = Energies{.specular = specEnergy, .diffuse = diffEnergy};
    }

    TreeNode newNode = TreeNode{
        .coordinates = newNodeInfo.coordinates,
        .rayDirection = normalizedDir,
        .normal = newNodeInfo.normal,
        .length = parentLength + newNodeInfo.length,
        .firstRayDirection = utils::reverseSphericalVector(Eigen::Vector3f(1.f, atan2(normalizedDir[1], normalizedDir[0]), acos(normalizedDir[2]))),
        .outgoingEnergies = outgoingEnergies,
        .audioMat = newNodeInfo.material,
        .reflectionTypes = reflectionTypes
        };
    return newNode;
}

void utils::computeSoundEvents(const std::vector<bool>& visibilityResults, const std::vector<AudioSource>& sources, TreeNode& newNode)
{
    // The TreeNodes outgoing energy redicued by the energy carried by the soundEvents.
    std::map<int, Energies> newOutgoingEnergies = newNode.outgoingEnergies;
    for (uint32_t i = 0; i < sources.size(); ++i)
    {
        if (visibilityResults[i])
        {
            SpecAndDiff energyProportions =
                utils::energyProportionToSource(sources[i].getRadius(), newNode.coordinates, sources[i].getPos(), newNode.rayDirection, newNode.normal);

            // Calculate the energy that the soundevent carries
            varays::energy soundEventEnergies;
            for (auto& [sourceID, globalEnergy] : newNode.outgoingEnergies)
            {
                // Specular energy
                for (auto& [specFrequency, specEnergy] : globalEnergy.specular)
                {
                    soundEventEnergies[specFrequency] = specEnergy * energyProportions.specular;
                    newOutgoingEnergies[sourceID].specular[specFrequency] -= specEnergy * energyProportions.specular;
                }
                // Diffuse energy
                for  (auto& [diffFrequency, diffEnergy] : globalEnergy.diffuse)
                {
                    soundEventEnergies[diffFrequency] += diffEnergy * energyProportions.diffuse;
                    newOutgoingEnergies[sourceID].diffuse[diffFrequency] -= diffEnergy * energyProportions.diffuse;
                }
            }

            // Transfer the information from a map to the energy vectors
            std::vector<float> frequencies, energies;
            float currentFreq = std::numeric_limits<float>::max();
            float minFreq = 0;
            float currentEnergy = 0;
            for (uint32_t i = 0; i < soundEventEnergies.size(); ++i)
            {
                for (auto energy = soundEventEnergies.cbegin(); energy != soundEventEnergies.cend(); ++energy)
                {
                    if (energy->first < currentFreq && energy->first > minFreq)
                    {
                        currentFreq = energy->first;
                        currentEnergy = energy->second;
                    }
                }
                frequencies.emplace_back(currentFreq);
                energies.emplace_back(currentEnergy);
                minFreq = currentFreq;
                currentFreq = std::numeric_limits<float>::max();
            }
            SoundEvent newSoundEvent = SoundEvent(SoundEvent::SoundEventConfig(sources[i].getId(), // source ID
                newNode.length + (sources[i].getPos() - newNode.coordinates).norm(), // length
                frequencies,
                energies,
                newNode.firstRayDirection, // First ray direction
                newNode.reflectionTypes));

            newNode.soundEvents.emplace_back(newSoundEvent);
        }
    }
    newNode.outgoingEnergies = newOutgoingEnergies;
}

float utils::visibilityAngleProportion(float radius, const Eigen::Vector3f& origin, const Eigen::Vector3f& source)
{
    double length = (origin - source).norm();
    return 0.5 * (1.0 - (length / (sqrt(pow(radius, 2.0) + pow(length, 2.0)))));
}

utils::SpecAndDiff utils::energyProportionToSource(
    float radius, const Eigen::Vector3f& origin, const Eigen::Vector3f& source, const Eigen::Vector3f& raydir, const Eigen::Vector3f& normal)
{
    // If the ray and the normal have less than 180 degrees between them
    if (raydir.dot(normal) > 0)
    {
        return SpecAndDiff{.specular = 0.0, .diffuse = 0.0};
    }
    Eigen::Vector3f toSrcDir = source - origin;
    // If the ray going towards the source and the normal have more than 180 degrees between them
    if (toSrcDir.dot(normal) < 0)
    {
        return SpecAndDiff{.specular = 0.0, .diffuse = 0.0};
    }
    float lengthToSource = toSrcDir.norm();
    toSrcDir /= lengthToSource; // Normalized direction
    float lengthOfPrism = toSrcDir.dot(normal);
    float widthOfPrism = (lengthOfPrism / lengthToSource) * radius;
    // 0.63661977236 is the volume of a sphere with 1 as a diameter
    // (pow(widthOfPrism, 2) * lengthOfPrism / 3.f) is the volume of the prism
    float proportionOfDiffuse = std::min((pow(widthOfPrism, 2.f) * lengthOfPrism / 3.f) / 0.63661977236, 1.0);
    float anglePrism = atan((widthOfPrism / 2.f) / lengthOfPrism);
    float angleSpecularAndSrc = acos((raydir - 2.0 * normal.dot(raydir) * raydir).dot(toSrcDir));
    float proportionOfSpecular = 0.f;

    if (angleSpecularAndSrc < anglePrism)
    {
        proportionOfSpecular = 1.f;
    }
    return SpecAndDiff{.specular = proportionOfSpecular, .diffuse = proportionOfDiffuse};
}
}; // namespace varays
