// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef __VARAYS_CONTEXT__
#define __VARAYS_CONTEXT__

#include <map>
#include <memory>
#include <unordered_map>
#include <vector>
#include <complex.h>
#undef I // This line is required to prevent conflict between "I" in complex.h and the "I" in spdlog (/external/spdlog/include/spdlog/fmt/bundled/core.h)

#include <Eigen/Dense>
#include <embree3/rtcore.h>

#include "./../Utils/Logger.hpp"
#include "./RayTracer/RayManager.hpp"
#include "./Scene/3D-structs.hpp"
#include "./Scene/ObjLoader.hpp"

namespace varays
{

struct MaterialProperty
{
    MaterialProperty(const std::string& material, const std::string& property, float frequency, float value)
        : material_(material)
        , property_(property)
        , frequency_(frequency)
        , value_(value){};

    std::string material_;
    std::string property_;
    float frequency_;
    float value_;
};

class VaraysContext
{

  public:
    /**
    * Constructor
    * 
    * Sets up the vaRays context, which is in charge managing everything related to the geometry and the ray tracing
    * It computes sets of soundEvents that can be used to construct the IR
    *
    * @param materialFilePath  Path of the file used to define materials
    * @param listenerPos       Original position of the first listener
    * @param cacheEnabled      Whether the rayTracing uses previous generations of thrown rays for coordinates that haven't changed
    * @param sources           List of initial sources present in the scene
    */
    VaraysContext(
        const std::string& materialFilePath = "", const Eigen::Vector3f listenerPos = {.0f, .0f, .0f}, bool cacheEnabled = false, const std::vector<AudioSource> sources = {});
    
    /**
    * Destructor
    */
    ~VaraysContext();

    /**
    * Adds a geometry to the scene that will be used for rayTracing
    *
    * @param vertices       Coordinates of the vertices of the geometry
    * @param indices        Indices of the vertices, length of the vector should be the same as vertices
    * @param diffraction    Whether diffraction detectors have to be generated for this geometry
    * @param matName        Name of the material used for the geometry. 
    *                       The name of that material should exist in the material file defined when creating the context.
    * 
    * @return               ID attributed to the geometry
    */
    unsigned int addGeom(
        const std::vector<Eigen::Vector3f>& vertices, const std::vector<int>& indices, bool diffraction = true, const std::string& matName = "Default");

    /**
    * Registers a 3D model in the context that can be added to the scene when needed
    *
    * @param vertices                       Coordinates of the vertices of the model
    * @param indices                        Indices of the vertices, length of the vector should be the same as vertices
    * @param modelKey                       ID that should be unique to this model
    * @param diffraction                    Whether diffraction detectors have to be generated for this model
    * @param instantiate                    Whether the model should be added to the scene right away
    * @param NeutralTransformationMatrix    Transformation matrix applied to this model before adding it to the scene
    * @param matName                        Name of the material used for this model.
    *                                       The name of that material should exist in the material file defined when creating the context.
    * 
    * @return                               ID attributed to the model
    */
    unsigned int addModel(const std::vector<Eigen::Vector3f>& vertices,
        const std::vector<int>& indices,
        unsigned int modelKey,
        bool diffraction = false,
        bool instantiate = false,
        const TransformationMatrix& mat = NeutralTransformationMatrix,
        const std::string& matName = "Default");


    /**
    * Creates a ray tracing scene from a previously registered model
    * 
    * @param modelKey   unique ID that was registered for this model
    * @param mat        transformation matrix that will be applied to the model before creating the scene with it. It will not affect the saved model
    * @return           ID attributed to the created scene
    */
    unsigned int instantiateModel(unsigned int modelKey, const TransformationMatrix& mat = NeutralTransformationMatrix);

    /**
    * Applies a transformation matrix to the whole ray tracing scene
    * 
    * @param sceneId   ID of the scene that will be affected
    * @param mat       transformation matrix that will be applied to the scene
    */
    void setTransform(unsigned int sceneId, const TransformationMatrix& mat);

    /**
    * Reads a .obj file and loads the ray tracing scene from it
    * 
    * @param path           path of the obj file
    * @param diffraction    whether diffraction detectors should be traced for this scene
    * @return               whether the reading of the file was successful
    */
    bool readObjFile(const std::string& path, bool diffraction = true);

    /**
    * Adds a new material to the list of materials that can be assigned to geometries in the scene
    * 
    * @param name           Name of the material (should be unique)
    * @param absorption     absorption coefficients
    * @param transmission   transmission coefficients
    * @param scattering     scattering coefficients
    * @return               whether registering of the file was successful
    */
    bool loadMat(const std::string& name,
        const std::map<float, float>& absorption,
        const std::map<float, float>& transmission,
        const std::map<float, float>& scattering);

    /**
    * Get the ID of a source from its name. Will return 0 if the source does not exist.
    * 
    * @param name   Name of the source
    * @return       ID of the source. 0 if the source doesn't exist.
    */
    unsigned int getSourceIDFromName(const std::string& name);

    /**
    * Creates and adds a new source to the scene
    * 
    * @param srcPos Initial position of the new source
    * @param name   Name of the source
    * @return       ID of the new source
    */
    unsigned int addSource(const Eigen::Vector3f& srcPos, const std::string& name = "");

    /**
    * Adds a new source to the scene
    * 
    * @param AudioSource    Previously instanciated source
    * @return               Whether adding the source was successful
    */
    bool addSource(const AudioSource& newSource);

    /**
    * Gets the current position of the listener in the scene
    * 
    * @return listener position
    */
    Eigen::Vector3f getListenerPos() const { return listener_; }

    /**
    * Moves the listener in the scene. The movement is instant, and take effect when the next ray generation is calculated.
    * 
    * @param newPos new coordinates of the listener
    */
    void moveListener(const Eigen::Vector3f& newPos);

    /**
    * Moves a source in the scene. The movement is instant, and take effect when the next ray generation is calculated.
    * 
    * @param id     ID of the moved source
    * @param newPos new coordinates of the source
    */
    bool moveSource(unsigned int id, const Eigen::Vector3f& newPos);

    /**
    * Removes a source from the scene. Takes effect when the next ray generation is calculated.
    * 
    * @param id     ID of the source to remove
    */
    bool removeSource(unsigned int id);

    /**
    * Get the source objects currently used for ray tracing
    * 
    * @return   Set of sources currently used for ray tracing
    */
    std::vector<AudioSource> getSources() const { return sources_; }

    /**
    * Get the source object from a source ID
    * 
    * @param i  ID of the source
    * @return   Source object
    */
    AudioSource getSource(unsigned int i) const;

    /**
    * Set frequencies for which the energy will be calculated for every soundEvent
    * 
    * @param frequencies    Set of frequencies (recommended to be between approximately 20 and 24000)
    */
    void setFrequencies(const std::vector<float>& frequencies);

    /**
    * Get frequencies at which energy is calculated for every soundEvent
    * 
    * @return Set of frequencies
    */
    std::vector<float> getFrequencies() const { return frequencies_; };

    /**
    * Traces the last generation of thrown rays in a .obj file.
    * 
    * @param path   Path of the .obj file that will be written
    */
    void writeRaysToObj(const std::string& path);

    /**
    * Adds a ray manager that will be in charge of throwing rays within specific limits
    * 
    * @param minReflectionOrder   Minimum reflection order for which the ray manager will compute rays
    * @param maxReflectionOrder   Maximum reflection order for which the ray manager will compute rays
    * @param targetFrequency      Ideal at which we want the rayManager to operate, generating new soundEvents every X (in seconds)
    * @param terminationEnergy    Energy threshold after which rays will stop pursuing their path, even if the maximum reflection order hasn't been reached
    */
    void addRayManager(int minReflectionOrder, int maxReflectionOrder, float targetFrequency, float terminationEnergy)
    {
        rayManagers_.emplace_back(minReflectionOrder, maxReflectionOrder, targetFrequency, terminationEnergy, embreeManager_.get());
    };

    /**
    * Executes the ray managers. Each of them will generate a new set of soundEvents.
    * 
    * @param rays           Initial amount of rays thrown
    * @param breakpoint     Reflection order after which only specular reflections are thrown
    * @param nReflections   Amount of rays thrown from every reflection point before orderBreakpoint is reached (1 will only throw a specular reflection)
    * @return SoundEvents generated by the visibility tests from the collision points found in this generation of rays
    */
    std::vector<SoundEvent> executeRayManagers(int rays, int breakpoint, int nReflections)
    {   
        std::vector<SoundEvent> soundEvents;
        for (auto& rayManager : rayManagers_)
        {
            std::vector<SoundEvent> genSoundEvents = rayManager.GetGeneration(listener_, sources_, rays, breakpoint, nReflections, scene_);
            soundEvents.insert(soundEvents.end(), genSoundEvents.begin(), genSoundEvents.end());
        }
        return soundEvents;
    };

    /**
    * Generates the direct soundevent for a single source
    * 
    * @param sourceID Source for which the direct sound will be generated
    * @return SoundEvent that corresponds to the direct sound. If there is no direct sound, the SoundEvent will be empty.
    */
    SoundEvent computeDirectSoundEvent(int sourceID)
    {
        if (rayManagers_.size() > 0)
        {   
            auto soundEvents = rayManagers_[0].ComputeDirectSounds(std::vector<varays::AudioSource>{getSource(sourceID)}, scene_, listener_);
            return (soundEvents.size() > 0) ? soundEvents[0] : SoundEvent();
        }
        return SoundEvent();
    }

    Logger logger_{"VaraysContext"};

    RTCScene scene_;

  private:
    float energyFromRays_{1.0f};
    RTCDevice device_;
    Eigen::Vector3f listener_;
    bool cacheEnabled_;
    std::vector<float> frequencies_{125, 250, 500, 1000, 2000, 4000, 8000, 16000};
    std::vector<AudioSource> sources_{};
    std::unordered_map<unsigned int, RTCScene> models_{};
    std::unordered_map<std::string, AudioMat> materials_{};
    std::vector<std::unique_ptr<GeomData>> geomDatas_{};

    std::unique_ptr<EmbreeManager> embreeManager_{std::make_unique<EmbreeManager>()};
    std::vector<RayManager> rayManagers_;

    static const float DiffractionThreshold;
    static const float PositionUpdateThreshold;

    unsigned int sourceIDCounter_ = 0;

    std::string exportObjFileName = "rebounds.obj";

    // for every ray, 2 sets of coordinates
    std::vector<std::vector<Eigen::Vector3f>> rayLines_;

    void initialize(std::vector<AudioSource> sources);
    void addDiffraction(const Eigen::Vector3f point1,
        const Eigen::Vector3f& point2,
        unsigned int parentID,
        RTCScene scene,
        const std::string& matName = "Default",
        float size = 0.05f);
    void loadDefaultMat();
    void loadMaterialsFromFile(const std::string& matFilePath);
};
}; // namespace varays
#endif
