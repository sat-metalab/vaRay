// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef __VARAYS_ENCODER__
#define __VARAYS_ENCODER__

#include <memory>
#include <vector>

#include <Eigen/Dense>

#include "../Common/SoundEvent.hpp"
#include "../Utils/Logger.hpp"
#include "./LRTree.hpp"

namespace varays
{

struct WeightedSoundEvents
{
    WeightedSoundEvents(const std::vector<SoundEvent>& soundEvents, float weight)
        : soundEvents_(soundEvents)
        , weight_(weight){};

    std::vector<SoundEvent> soundEvents_;
    float weight_;
};

class VaraysEncoder
{
  public:
    Logger logger_{"VaraysEncoder"};

    /*
     * Constructor
     *
     * Sets up Linkwitz-Riley filter tree for IR generation and computes
     * attenutation coefficients for the given atmospheric conditions.
     *
     * @param frequencyBands  Centre frequencies of IR synthesis filter bank
     * @param fs              Sample rate of operation
     */
    VaraysEncoder(const std::vector<float>& frequencyBands, int sampleRate);

    /*
     * Constructor
     *
     * Sets up Linkwitz-Riley filter tree for IR generation and computes
     * attenutation coefficients for the given atmospheric conditions. The default
     * sample rate used is 48000Hz. Use the other overloaded function to modify the 
     * sample rate.
     *
     * @param frequencyBands  Centre frequencies of IR synthesis filter bank
     */
    VaraysEncoder(const std::vector<float>& frequencyBands): VaraysEncoder(frequencyBands, 48000) {}

    /*
     * Synthesize ambisonic room impulse responses for the scene
     *
     * This function computes the room impulse responses for each sources in
     * the scene given the rays between the sources and the listener.
     *
     * @param  ambiOrder    Required ambisonic order of the impulse responses
     * @param  soundEvents  Vector of SoundEvents in the scene. Represents rays
     *                      incident from the sources to the listener.
     *
     * @return Map of sources and their impulse responses.
     */
    std::map<unsigned int, std::vector<float>> writeIR(int ambiOrder, const std::vector<SoundEvent>& soundEvents, bool includeDirectSound = true);

    std::vector<SoundEvent> interpolateSoundEvents(const std::vector<WeightedSoundEvents>& soundEvents);

    /*
     * Set relative humidity of the atmosphere
     *
     * @param  humidity  Relative humidity in %
     */
    void setHumidity(float humidity) { setAtmosphericConditions(humidity, pressure_, temperature_); }

    /*
     * Set atmospheric pressure
     *
     * @param  pressure  Atmospheric pressure in kPa
     */
    void setPressure(float pressure) { setAtmosphericConditions(humidity_, pressure, temperature_); }

    /*
     * Set temperature of the atmosphere
     *
     * @param  temperature Temperature in Celcius scale
     */
    void setTemperature(double temperature) { setAtmosphericConditions(humidity_, pressure_, temperature); }

    /*
     * Set atmospheric conditions
     *
     * Sets relative humidity, atmospheric pressure and temperature of the scene.
     *
     * @param  humidity     Relative humidity in %
     * @param  pressure     Atmospheric pressure in kPa
     * @param  temperature  Temperature in Celcius scale
     */
    void setAtmosphericConditions(float humidity, float pressure, double temperature);

    /*
     * Set sample rate of IRs to be computed
     *
     * @param sampleRate Sample rate in Hz
     */
    void setSampleRate(int sampleRate);

    /*
     * Get air absorption coefficients for the given propagation distance
     *
     * This function computes the air absorption coefficients for the given distance
     * of propagation at the centre frequencies of the IR generation filter bank.
     *
     * @param  length  Distance of propagation in m
     * @return A row of air absorption coefficients for the different frequencies
     */
    Eigen::ArrayXd getAirAttenuation(float length);

    /*
     * Get speed of sound in air
     *
     * @return  Speed of sound in m/s
     */
    float getSpeedOfSound() const { return speedOfSound_; }

    /*
     * Export IR as a wav file
     *
     * @param  vData      Vector of interleaved samples of the IR
     * @param  path       Output path for the wav file
     * @param  channels   Number of ambisonic channels in the IR
     * @param  sampleRate Sample rate of the exported IR
     */
    void exportIR(const std::vector<float>& vData, const std::string& path, int channels, int sampleRate);

  private:

    // Encoding properties
    int sampleRate_{48000};

    // Meteorogical conditions
    float speedOfSound_{343.0};
    float humidity_{50.0}; /**< (0-100, % of humidity in air) */
    float pressure_{101.325}; /**< (in kPa (average pressure at sea level = 101.325)) */
    double temperature_{20.0}; /**< Celsius */
    std::vector<double> airAttenuations_; /**< Attenuation coefficients in dB per meters */

    /*
     * Precompute air absorption coefficients for unit distance
     *
     * This function computes the unit-distance air absorption coefficients in dB/m
     * for the given frequencies and stores them for later use.
     *
     * @param  frequencies Frequencies in Hz for which air aborption coefficients are to be computed
     */
    void initializeAirAttenuations(const std::vector<float>& frequencies);

    /*
     * Compute air absorption coefficient for the given frequency in dB/m
     *
     * Ref: https://en.wikibooks.org/wiki/Engineering_Acoustics/Outdoor_Sound_Propagation
     *
     * @param  frequency Frequency in Hz
     */
    double getAbsorptionForFrequency(float frequency); // in decibels

    /*
     * Compute speed of sound in air for the set atmospheric conditions
     */
    void updateSpeedOfSound(); // Should be called every time a meteorologic property changes

    std::unique_ptr<LRTree> irFilter_{}; /**< Linkitz-Riley filter tree */
};
}; // namespace varays
#endif
