#include <pybind11/eigen.h>
#include <pybind11/operators.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <vector>

#include <Eigen/Dense>

#include "./varays/Auralizer/VaraysAuralizer.hpp"
#include "./varays/Common/SoundEvent.hpp"
#include "./varays/Context/VaraysContext.hpp"
#include "./varays/Encoder/VaraysEncoder.hpp"

using namespace varays;
namespace py = pybind11;

PYBIND11_MODULE(pyvarays, m)
{
    m.doc() = R"pbdoc(
        Python wrapper for vaRays
        -------------------------

        .. currentmodule:: varays

        .. autosummary::
            :toctree: _generate
            
    )pbdoc";

    // WRAPPED CLASSES
    /**
     * AudioSource
     */
    py::class_<AudioSource>(m, "AudioSource")
        .def(py::init<unsigned int, const std::string&, const Eigen::Vector3f&, const std::vector<float>&, float>())
        .def("get_id", &AudioSource::getId)
        .def("get_name", &AudioSource::getName)
        .def("get_pos", &AudioSource::getPos);

    /**
     * SoundEvent
     */
    py::enum_<SoundEvent::Reflection>(m, "Reflection")
      .value("SPECULAR", SoundEvent::Reflection::SPECULAR)
      .value("DIFFUSION", SoundEvent::Reflection::DIFFUSION)
      .value("DIFFRACTION", SoundEvent::Reflection::DIFFRACTION)
      .value("TRANSMISSION", SoundEvent::Reflection::TRANSMISSION);

    py::class_<SoundEvent::SoundEventConfig>(m, "SoundEventConfig")
      .def(py::init<unsigned int, float, const std::vector<float>&,
          const std::vector<float>&, const Eigen::Vector3f&,
          const std::vector<SoundEvent::Reflection>&>(),
          py::arg("source_id") = 0,
          py::arg("length") = 0,
          py::arg("frequencies") = std::vector<float>(),
          py::arg("energies") = std::vector<float>(),
          py::arg("spherical_coordinates") = Eigen::Vector3f(0., 0., 0.),
          py::arg("reflections") = std::vector<SoundEvent::Reflection>()
      )
      .def_readwrite(
          "source_id",
          &SoundEvent::SoundEventConfig::sourceID
      )
      .def_readwrite(
          "length",
          &SoundEvent::SoundEventConfig::length
      )
      .def_readonly(
          "frequencies",
          &SoundEvent::SoundEventConfig::frequencies
      )
      .def_readonly(
          "energies",
          &SoundEvent::SoundEventConfig::energies
      )
      .def_readonly(
          "spherical_coordinates",
          &SoundEvent::SoundEventConfig::sphericalCoordinates
      )
      .def_readonly(
          "reflections",
          &SoundEvent::SoundEventConfig::reflections
      );

    py::class_<SoundEvent>(m, "SoundEvent")
        .def(py::init<SoundEvent::SoundEventConfig>())
        .def_readwrite("source_id", &SoundEvent::sourceID_)
        .def_readwrite("length", &SoundEvent::length_)
        .def_readwrite("frequencies", &SoundEvent::frequencies_)
        .def_readwrite("energies", &SoundEvent::energies_)
        .def_readwrite("spherical_coordinates", &SoundEvent::sphericalCoordinates_)
        .def_readwrite("reflections", &SoundEvent::reflections_);

    /** 
     * VaraysContext
     */
    py::class_<VaraysContext>(m, "VaraysContext")
        .def(py::init<const std::string&, const Eigen::Vector3f, bool,
            const std::vector<AudioSource>>(),
            py::arg("material_file_path") = "",
            py::arg("listener_position") = Eigen::Vector3f(.0f, .0f, .0f),
            py::arg("cache_enabled") = false,
            py::arg("audio_sources") = std::vector<AudioSource>()
        )
        .def("add_geom", &VaraysContext::addGeom,
            py::arg("vertices"),
            py::arg("indices"),
            py::arg("diffraction") = true,
            py::arg("material_name") = "Default"
        )
        .def("add_source",
            py::overload_cast<const Eigen::Vector3f&, const std::string&>(&VaraysContext::addSource),
            py::arg("source_position"),
            py::arg("name") = ""
        )
        .def("get_frequencies", &VaraysContext::getFrequencies)
        .def("get_sources", &VaraysContext::getSources)
        .def("get_source_id_from_name", &VaraysContext::getSourceIDFromName,
            py::arg("name")
        )
        .def("get_sources", &VaraysContext::getSources)
        .def("add_ray_manager", &VaraysContext::addRayManager)
        .def("execute_ray_managers", &VaraysContext::executeRayManagers)
        .def("move_source", &VaraysContext::moveSource)
        .def("read_obj_file", &VaraysContext::readObjFile)
        .def("remove_source", &VaraysContext::removeSource)
        .def("write_rays_to_obj", &VaraysContext::writeRaysToObj);

    /* VaraysAuralizer
     *
     */
    // VaraysAuralizer::ERROR_CODE
    py::enum_<VaraysAuralizer::ERROR_CODE>(m, "ERROR_CODE")
        .value("NOERR", VaraysAuralizer::ERROR_CODE::NOERR)
        .value("ERR_OTHER", VaraysAuralizer::ERROR_CODE::ERR_OTHER)
        .value("ERR_SYNTAX", VaraysAuralizer::ERROR_CODE::ERR_SYNTAX)
        .value("ERR_PARAM", VaraysAuralizer::ERROR_CODE::ERR_PARAM)
        .value("ERR_ALLOC", VaraysAuralizer::ERROR_CODE::ERR_ALLOC)
        .value("ERR_CANTCD", VaraysAuralizer::ERROR_CODE::ERR_CANTCD)
        .value("ERR_COMMAND", VaraysAuralizer::ERROR_CODE::ERR_COMMAND)
        .value("ERR_INCONV", VaraysAuralizer::ERROR_CODE::ERR_INCONV)
        .value("ERR_NOCONV", VaraysAuralizer::ERROR_CODE::ERR_NOCONV)
        .value("ERR_IONUM", VaraysAuralizer::ERROR_CODE::ERR_IONUM)
        .export_values();

    // VaraysAuralizerParams
    py::class_<VaraysAuralizer::VaraysAuralizerParams>(m, "VaraysAuralizerParams")
        .def(py::init<const std::string&, unsigned int, unsigned int, unsigned int, unsigned int, bool, unsigned int, float, float>(),
            py::arg("jack_server") = "default",
            py::arg("num_inputs") = 1,
            py::arg("num_outputs") = 1,
            py::arg("convolver_size") = 104800,
            py::arg("convolver_partition_size") = 256,
            py::arg("enable_doppler") = true,
            py::arg("doppler_interpolation_order") = 1,
            py::arg("doppler_max_velocity") = 50.0f,
            py::arg("doppler_speed_of_sound") = 343.0f)
        .def_readwrite("jack_server", &VaraysAuralizer::VaraysAuralizerParams::jackServerName_)
        .def_readwrite("num_inputs", &VaraysAuralizer::VaraysAuralizerParams::numInputs_)
        .def_readwrite("num_outputs", &VaraysAuralizer::VaraysAuralizerParams::numOutputs_)
        .def_readwrite("convolver_size", &VaraysAuralizer::VaraysAuralizerParams::convSize_)
        .def_readwrite("convolver_partition_size", &VaraysAuralizer::VaraysAuralizerParams::convPartitionSize_)
        .def_readwrite("enable_doppler", &VaraysAuralizer::VaraysAuralizerParams::enableDoppler_)
        .def_readwrite("doppler_interpolation_order", &VaraysAuralizer::VaraysAuralizerParams::dopplerInterpOrder_)
        .def_readwrite("doppler_max_velocity", &VaraysAuralizer::VaraysAuralizerParams::dopplerMaxVelocity_)
        .def_readwrite("doppler_speed_of_sound", &VaraysAuralizer::VaraysAuralizerParams::dopplerSpeedOfSound_);

    // VaraysAuralizer
    py::class_<VaraysAuralizer>(m, "VaraysAuralizer")
        .def(py::init<VaraysAuralizer::VaraysAuralizerParams>(),
            py::arg("varays_auralizer_params") = VaraysAuralizer::VaraysAuralizerParams()
        )
        .def("delete_source", &VaraysAuralizer::deleteSource)
        .def("get_sample_rate", &VaraysAuralizer::getSampleRate)
        .def("is_allocated", &VaraysAuralizer::isAllocated)
        .def("new_source", &VaraysAuralizer::newSource)
        .def("set_vectorized_operations", &VaraysAuralizer::setVectorizedOperations)
        .def("update_ir", &VaraysAuralizer::updateIR);

    /* VaraysEncoder
     *
     */
    py::class_<VaraysEncoder>(m, "VaraysEncoder")
        .def(py::init<const std::vector<float>&, int>())
        .def("export_ir", &VaraysEncoder::exportIR)
        .def("get_speed_of_sound", &VaraysEncoder::getSpeedOfSound)
        .def("set_atmospheric_conditions", &VaraysEncoder::setAtmosphericConditions)
        .def("set_sample_rate", &VaraysEncoder::setSampleRate)
        .def("write_ir", &VaraysEncoder::writeIR,
            py::arg("ambisonic_order"),
            py::arg("sound_events"),
            py::arg("include_direct_sound") = true
        );

    // HELPER FUNCTIONS
    /* Context::RayTracer::utils
     *
     */
    m.def("sphere_random", utils::sphereRandom);
}
